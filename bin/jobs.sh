#!/bin/sh

if [ "$1" = "stop" ] ; then
	ps -ef|grep kettle|grep -v grep|awk  '{print "kill -9 " $2}' |sh
elif [ "$1" = "start" ]; then
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/tzlc.kjb > /opt/logs/tzlc_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/whpj.kjb > /opt/logs/whpj_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/ll.kjb > /opt/logs/ll_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/hj.kjb > /opt/logs/hj_job.log &
elif [ "$1" = "restart" ]; then
	ps -ef|grep kettle|grep -v grep|awk  '{print "kill -9 " $2}' |sh
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/tzlc.kjb > /opt/logs/tzlc_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/whpj.kjb > /opt/logs/whpj_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/ll.kjb > /opt/logs/ll_job.log &
	sleep 1
	/opt/data-integration/kitchen.sh /norep /file /opt/data-integration/data/hj.kjb > /opt/logs/hj_job.log &
fi