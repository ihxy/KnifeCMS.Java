package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_grab_news", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class GrabNews {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;
	
	@TableField(name = "k_source")
	private String source;
	
	@TableField(name = "k_author")
	private String author;
	
	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_date")
	private Date date;
	
	@TableField(name = "k_content")
	private String content;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setGrabNews(com.knife.news.object.GrabNews news){
    	this.id=news.getId();
	}
}
