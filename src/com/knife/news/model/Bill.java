package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_bill", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Bill {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_suser")
	private String suser;
	
	@TableField(name = "k_file")
	private String file;
	
	@TableField(name = "k_date")
	private Date date;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    
	public String getSuser() {
		return suser;
	}
	
	public void setSuser(String suser) {
		this.suser = suser;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setBill(com.knife.news.object.Bill news){
    	this.id=news.getId();
    	this.suser=news.getSuser();
    	this.file=news.getFile();
    	this.date=news.getDate();
	}
}
