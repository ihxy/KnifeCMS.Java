package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_flow", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Flow {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_site")
	private String site;

	@TableField(name = "k_date")
	private Date date;
	
	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_status")
	private int status;
	
	@TableField(name = "k_description")
	private String description;
	
	@TableField(name = "k_type")
	private int type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setFlow(com.knife.news.object.Flow flow){
		this.id		= flow.getId();
		this.date	= flow.getDate();
		this.site	= flow.getSite();
		this.name	= flow.getName();
		this.url	= flow.getUrl();
		this.status	= flow.getStatus();
		this.description = flow.getDescription();
		this.type	= flow.getType();
	}
}
