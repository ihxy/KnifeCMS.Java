package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.ContentService;
import com.knife.news.object.Content;

public class ContentServiceImpl extends DAOSupportService implements ContentService {
	private static ContentService contentDAO = new ContentServiceImpl();

	public static ContentService getInstance() {
		return contentDAO;
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#addContent(com.knife.news.object.Content)
	 */
	public boolean addContent(Content news) {
		com.knife.news.model.Content anews = new com.knife.news.model.Content();
		news.setId(anews.getId());
		anews.setContent(news);
		if (dao.save(anews)) {
			return true;
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#saveContent(com.knife.news.object.Content)
	 */
	public boolean saveContent(Content content) {
		com.knife.news.model.Content acontent = new com.knife.news.model.Content();
		acontent.setContent(content);
		return this.dao.save(acontent);
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#updateContent(com.knife.news.object.Content)
	 */
	public boolean updateContent(Content content) {
		com.knife.news.model.Content acontent = new com.knife.news.model.Content();
		acontent.setContent(content);
		return this.dao.update(acontent);
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#delContent(com.knife.news.object.Content)
	 */
	public boolean delContent(Content content) {
		com.knife.news.model.Content acontent = new com.knife.news.model.Content();
		acontent.setContent(content);
		return this.dao.del(acontent);
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#getContent()
	 */
	public List<Content> getContent() {
		return changeListType(this.dao.query(
				com.knife.news.model.Content.class, "id!=''"));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#getContentByNewsid(java.lang.String)
	 */
	public List<Content> getContentByNewsid(String newsid) {
		List<Object> paras = new ArrayList<Object>();
		String sql = "id!=''";
		if (newsid.length() > 0) {
			paras.add(newsid);
			sql += " and k_news_id=?";
		}
		return changeListType(this.dao.query(
				com.knife.news.model.Content.class, sql, paras));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#getContent(java.lang.String, java.util.Collection)
	 */
	public List<Content> getContent(String sql, Collection<Object> paras) {
		return changeListType(this.dao.query(
				com.knife.news.model.Content.class, sql, paras));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#getContentById(java.lang.String)
	 */
	public Content getContentById(String id) {
		try {
			com.knife.news.model.Content content = (com.knife.news.model.Content) this.dao
					.get(com.knife.news.model.Content.class, id);
			return (new Content(content));
		} catch (Exception e) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.ContentService#getCommBySql(java.lang.String, java.util.Collection, int, int)
	 */
	public List<Content> getCommBySql(String sql, Collection<Object> paras,
			int begin, int end) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(
				com.knife.news.model.Content.class, sql, paras, begin, end));
	}

	public List<Content> changeListType(List<Object> objs) {
		List<Content> results = new ArrayList<Content>();
		if (objs != null) {
			for (Object newobj : objs) {
				com.knife.news.model.Content robj = (com.knife.news.model.Content) newobj;
				Content tobj = new Content(robj);
				// Content robj = (Content) newobj;
				results.add(tobj);
			}
		}
		return results;
	}
}