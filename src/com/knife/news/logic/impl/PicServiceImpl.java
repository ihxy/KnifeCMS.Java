package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.PicService;
import com.knife.news.object.Pic;
import com.knife.news.object.Pic;
import com.knife.util.CommUtil;

public class PicServiceImpl extends DAOSupportService implements PicService {
	private static PicServiceImpl picDAO = new PicServiceImpl();

	public static PicServiceImpl getInstance() {
		return picDAO;
	}

	public List<Pic> getAllPics(){
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "id!=''"));
	}
	
	public List<Pic> getPicsByType(String typeid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(typeid);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_newsid in(select id from k_news where k_type=?)",paras));
	}
	
	public List<Pic> getPicsByPicType(String pictypeid) {
		return getPicsByPicType(pictypeid,-1);
	}
	
	public List<Pic> getPicsByPicType(String pictypeid,int display) {
		String sql="k_pic_type=?";
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(pictypeid);
		if(display>0){
			sql+=" and k_display=?";
			paras.add(display);
		}
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, sql,paras));
	}
	
	public List<Pic> getPicsByTree(String treeid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(treeid);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_newsid in(select id from k_news where k_pic in(select id from k_pic where k_tree_id=?))",paras));
	}
	
	public List<Pic> getPicsByUserId(int userid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(userid);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_userid=? order by length(k_order),k_order",paras));
	}

	public List<Pic> getPicsByNewsId(String newsid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_newsid=? order by length(k_order),k_order",paras));
	}
	
	public List<Pic> getPicsByNewsId(String newsid,String orderby) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_newsid=? order by "+orderby,paras));
	}

	public List<Pic> getPicsBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, sql));
	}

	public List<Pic> getPicsBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, sql, paras, begin, end));
	}

	public Pic getPicById(String id) {
		com.knife.news.model.Pic apic=(com.knife.news.model.Pic)this.dao.get(com.knife.news.model.Pic.class, id);
		return new Pic(apic);
	}
	
	public List<Pic> getPicsByUrl(String url){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(url);
		return changeListPic(this.dao.query(com.knife.news.model.Pic.class, "k_url=?",paras));
	}

	public boolean savePic(Pic pic) {
		com.knife.news.model.Pic bpic=new com.knife.news.model.Pic();
		bpic.setPic(pic);
		return this.dao.save(bpic);
	}

	public boolean updatePic(Pic pic) {
		com.knife.news.model.Pic bpic=new com.knife.news.model.Pic();
		bpic.setPic(pic);
		return this.dao.update(bpic);
	}

	public boolean delPic(Pic pic) {
		com.knife.news.model.Pic bpic=new com.knife.news.model.Pic();
		bpic.setPic(pic);
		return this.dao.del(bpic);
	}
	
	public List<Pic> changeListPic(List<Object> pic){
		List<Pic> results = new ArrayList<Pic>();
		if(pic!=null){
			for (Object newobj : pic) {
				com.knife.news.model.Pic bnews = (com.knife.news.model.Pic) newobj;
				Pic apic=new Pic(bnews);
				results.add(apic);
			}
		}
		return results;	
	}
	
	public void reArrange(Pic oldPic , int newOrder) {
		String newsid=oldPic.getNewsid();
		if(newsid.length()>0){
			if (newOrder > 0) {
				List<Pic> pics = this.getPicsByNewsId(newsid);
				int maxOrder = pics.size();
				if (newOrder > maxOrder) {
					newOrder = maxOrder;
				}
				int oldOrder = oldPic.getOrder();
					//System.out.println("获取到的频道数:"+maxOrder);
					Collection<Object> paras = new ArrayList<Object>();
					String sql="1=1";
				if (!newsid.equals(oldPic.getNewsid())) {
					dao.execute("update k_pic set k_order=k_order-1 where "+sql+" and k_newsid='"+ oldPic.getNewsid() +"' and k_order>"+ oldOrder,paras);
					dao.execute("update k_pic set k_order=k_order+1 where "+sql+" and k_newsid='"+ newsid +"' and k_order>="+ newOrder,paras);
				} else {
					//System.out.println("获取到的频道数:"+maxOrder);
					if (oldOrder != newOrder) {
						pics = changeListPic(dao.query(com.knife.news.model.Pic.class, sql, paras));
						maxOrder = pics.size();
						if (newOrder > maxOrder) {
							newOrder = maxOrder;
						}
						if (oldOrder < newOrder) {
							
							dao
									.execute("update k_pic set k_order=k_order-1 where "+sql+" and k_newsid='"
											+ newsid
											+ "' and k_order<="
											+ newOrder
											+ " and k_order>="
											+ oldOrder,paras);
						} else {
							dao
									.execute("update k_pic set k_order=k_order+1 where "+sql+" and k_newsid='"
											+ newsid
											+ "' and k_order>="
											+ newOrder
											+ " and k_order<="
											+ oldOrder,paras);
						}
					}
				oldPic.setOrder(newOrder);
				com.knife.news.model.Pic apic=new com.knife.news.model.Pic();
				apic.setPic(oldPic);
				dao.update(apic);
				}
			}
		}
	}
}