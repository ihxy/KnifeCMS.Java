package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.CommentService;
import com.knife.news.object.Comment;

public class CommentServiceImpl extends DAOSupportService implements
		CommentService {
private static CommentServiceImpl commentDAO=new CommentServiceImpl();
public static CommentServiceImpl getInstance(){
	return commentDAO;
}

public boolean addComment(Comment news) {
	com.knife.news.model.Comment anews=new com.knife.news.model.Comment();
	news.setId(anews.getId());
	anews.setComment(news);
	if(dao.save(anews)){
		return true;
	}else{
		return false;
	}
}

	public boolean saveComment(Comment comment) {
		com.knife.news.model.Comment acomment=new com.knife.news.model.Comment();
		acomment.setComment(comment);
		return this.dao.save(acomment);
	}
	
	//jlm
	public boolean saveNewComment(Comment comment) {
		com.knife.news.model.Comment acomment=new com.knife.news.model.Comment();
		acomment.addComment(comment);
		return this.dao.save(acomment);
	}
	
	public boolean updateComment(Comment comment) {
		com.knife.news.model.Comment acomment=new com.knife.news.model.Comment();
		acomment.setComment(comment);
		return this.dao.update(acomment);
	}

	public boolean delComment(Comment comment) {
		com.knife.news.model.Comment acomment=new com.knife.news.model.Comment();
		acomment.setComment(comment);
		return this.dao.del(acomment);
	}

	public List<Comment> getComment() {
		return changeListType(this.dao.query(com.knife.news.model.Comment.class,"id!=''"));
	}
	
	public List<Comment> getCommentByNewsid(String newsid){
		List<Object> paras = new ArrayList<Object>();
		String sql="id!=''";
		if(newsid.length()>0){
			paras.add(newsid);
			sql+=" and k_news_id=?";
		}
		return changeListType(this.dao.query(com.knife.news.model.Comment.class,sql,paras));
	}
	
	public List<Comment> getComment(String sql, Collection<Object> paras) {
		return changeListType(this.dao.query(com.knife.news.model.Comment.class, sql, paras));
	}

	public Comment getCommentById(String id) {
		try{
			com.knife.news.model.Comment comment=(com.knife.news.model.Comment)this.dao.get(com.knife.news.model.Comment.class,id);
			return (new Comment(comment));
		}catch(Exception e){
			return null;
		}
	}
	public List<Comment> getCommBySql(String sql, Collection<Object> paras, int begin, int end) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.Comment.class,sql,paras,begin,end));
	}
	
	public List<Comment> changeListType(List<Object> objs){
		List<Comment> results = new ArrayList<Comment>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Comment robj = (com.knife.news.model.Comment) newobj;
				Comment tobj =  new Comment(robj);
				//Comment robj = (Comment) newobj;
				results.add(tobj);
			}
		}
		return results;
	}
}