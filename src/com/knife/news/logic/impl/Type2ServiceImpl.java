package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.Type2Service;
import com.knife.news.model.Type2;

public class Type2ServiceImpl extends DAOSupportService implements Type2Service {
	
	//private NewsServiceImpl newsDAO = new NewsServiceImpl();
	private static Type2ServiceImpl typedao = new Type2ServiceImpl();

	public static Type2ServiceImpl getInstance() {
		return typedao;
	}

	public boolean saveType(Type2 type) {
		return this.dao.save(type);
	}

	public boolean updateType(Type2 type) {
		return this.dao.update(type);
	}

	public boolean delType(Type2 type) {
		return this.dao.del(type);
	}
	
	/*public List<com.knife.news.object.Type> getAllTypes(int display){
		return getAllTypes("",display);
	}*/
	
	public int getSum(int display,String parentid,String tree_id){
		int sum=0;
		String sql = "select count(*) from k_type2 where id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(parentid.length()>0){
			paras.add(parentid);
			sql += " and k_parent=?";
		}
		if(tree_id.length()>0){
			paras.add(tree_id);
			sql += " and k_tree_id=?";
		}
		sum=Integer.parseInt(this.dao.uniqueResult(sql,paras).toString());
		return sum;
	}
	
	public List<com.knife.news.object.Type2> getAllTypes() {
		String sql = "id!=''";
		return changeListType(this.dao.query(Type2.class, sql,null));
	}

	public com.knife.news.object.Type2 getTypeById(String id) {
		com.knife.news.object.Type2 btype = new com.knife.news.object.Type2(
				(Type2) this.dao.get(Type2.class, id));
		return btype;
	}
	
	public com.knife.news.object.Type2 getTypeByName(String name) {
		Collection<Object> paras=new ArrayList<Object>();
		paras.add(name);
		String id=(String)this.dao.uniqueResult("select id from k_type where k_name=? order by length(k_parent),k_parent",paras);
		com.knife.news.object.Type2 btype = new com.knife.news.object.Type2(
				(Type2) this.dao.get(Type2.class, id));
		return btype;
	}

	public List<com.knife.news.object.Type2> getTypesBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(Type2.class, sql));
	}

	public List<com.knife.news.object.Type2> getTypesBySql(String sql, Collection<Object> paras, int begin, int end) {
		//List<com.knife.news.model.Type> types
		return changeListType(this.dao.query(Type2.class, sql, paras, begin, end));
	}
	
	public List<com.knife.news.object.Type2> changeListType(List<Object> objs){
		List<com.knife.news.object.Type2> results = new ArrayList<com.knife.news.object.Type2>();
		if(objs!=null){
			for (Object newobj : objs) {
				Type2 robj = (Type2) newobj;
				com.knife.news.object.Type2 tobj =  new com.knife.news.object.Type2(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}