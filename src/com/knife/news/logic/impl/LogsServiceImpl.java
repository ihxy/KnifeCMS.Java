package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.LogsService;
import com.knife.news.object.Logs;

public class LogsServiceImpl extends DAOSupportService implements LogsService {
	private static LogsServiceImpl logDao = new LogsServiceImpl();

	public static LogsServiceImpl getInstance() {
		return logDao;
	}

	public boolean saveLogs(Logs log) {
		com.knife.news.model.Logs alog=new com.knife.news.model.Logs();
		alog.setLogs(log);
		return this.dao.save(alog);
	}
	
	public boolean updateLogs(Logs log) {
		com.knife.news.model.Logs alog=new com.knife.news.model.Logs();
		alog.setLogs(log);
		return this.dao.update(alog);
	}
	
	public Logs getLogsById(String id) {
		try{
			com.knife.news.model.Logs alog = (com.knife.news.model.Logs) this.dao.get(com.knife.news.model.Logs.class, id);
			Logs blog = new Logs(alog);
			return blog;
		}catch(Exception e){
			return null;
		}
	}
	
	public boolean delLogs(Logs log) {
		com.knife.news.model.Logs alog=new com.knife.news.model.Logs();
		alog.setLogs(log);
		return dao.del(alog);
	}
	
	public boolean cleanLogs(){
		int ret= dao.execute("truncate table k_logs");
		if(ret>=0){
			return true;
		}else{
			return false;
		}
	}
	
	public List<Logs> getAllLogs() {
		return changeListType(this.dao.query(com.knife.news.model.Logs.class, "id!=''"));
		//return this.dao.query(News.class, "k_type=?", paras);
	}
	
	public List<Logs> getLogsBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Logs.class, sql, paras, begin, end));
	}
	
	public List<Logs> changeListType(List<Object> objs){
		List<Logs> results = new ArrayList<Logs>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Logs robj = (com.knife.news.model.Logs) newobj;
				Logs tobj =  new Logs(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}
