package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.knife.news.logic.PicTypeService;
import com.knife.news.logic.UserService;
import com.knife.news.object.PicType;
import com.knife.news.object.User;
import com.knife.util.CommUtil;

public class PicTypeServiceImpl extends DAOSupportService implements PicTypeService {
	private static PicTypeService pictypedao = new PicTypeServiceImpl();
	private UserService userDAO = UserServiceImpl.getInstance();

	public static PicTypeService getInstance() {
		return pictypedao;
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#savePicType(com.knife.news.object.PicType)
	 */
	
	public String savePicType(PicType type) {
		com.knife.news.model.PicType atype=new com.knife.news.model.PicType();
		type.setId(atype.getId());
		atype.setPicType(type);
		if(this.dao.save(atype)){
			return atype.getId();
		}else{
			return "";
		}
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#updatePicType(com.knife.news.object.PicType)
	 */
	
	public boolean updatePicType(PicType type) {
		com.knife.news.model.PicType atype=new com.knife.news.model.PicType();
		atype.setPicType(type);
		return this.dao.update(atype);
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#delPicType(com.knife.news.object.PicType)
	 */
	
	public boolean delPicType(PicType type) {
		dao.execute("delete from k_pic where k_pic_type='"+type.getId()+"'");
		dao.execute("update k_pic_type set k_order=k_order-1 where k_parent='"+type.getParent()+"' and k_order>"+type.getOrder());
		com.knife.news.model.PicType atype=new com.knife.news.model.PicType();
		atype.setPicType(type);
		return this.dao.del(atype);
	}
	
	/*public List<com.knife.news.object.PicType> getAllPicTypes(int display){
		return getAllPicTypes("",display);
	}*/
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSum(int, java.lang.String, java.lang.String)
	 */
	
	public int getSum(int display,String parentid,String tree_id){
		int sum=0;
		String sql = "select count(*) from k_pic_type where id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(parentid.length()>0){
			paras.add(parentid);
			sql += " and k_parent=?";
		}
		if(tree_id.length()>0){
			paras.add(tree_id);
			sql += " and k_tree_id=?";
		}
		sum=Integer.parseInt(this.dao.uniqueResult(sql,paras).toString());
		return sum;
	}
	
	//@Override
	public List<PicType> getPicTypesByUser(int userid) {
		return getAllPicTypes(userid,1);
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllPicTypes(int)
	 */
	//@Override
	public List<PicType> getAllPicTypes(int display) {
		String sql = "id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		sql += " order by length(k_order),k_order";
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql,paras));
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllPicTypes(java.lang.String)
	 */
	//@Override
	public List<PicType> getAllPicTypes(String siteid) {
		String sql = "id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(siteid.length()>0) {
			paras.add(siteid);
			sql += " and k_site=?";
		}
		sql += " order by length(k_order),k_order";
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql,paras));
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllPicTypes(java.lang.String, int)
	 */
	//@Override
	public List<PicType> getAllPicTypes(int userid,int display){
		return getAllPicTypes("", userid, display);
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllPicTypes(java.lang.String, java.lang.String, int)
	 */
	//@Override
	public List<PicType> getAllPicTypes(String siteid,int userid,int display) {
		String sql = "id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(siteid.length()>0){
			paras.add(siteid);
			sql += " and k_site=?";
		}
		if(userid>0){
			paras.add(userid);
			sql += " and k_userid=?";
		}
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		sql += " order by length(k_order),k_order";
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql,paras));
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllRightPicTypes(java.lang.String)
	 */
	//@Override
	public List<PicType> getAllRightPicTypes(String userid){
		String sql = "";
		Collection<Object> paras=new ArrayList<Object>();
		paras.add(userid);
		sql = "id!='' and k_admin like '%'||?||',%' order by length(k_order),k_order";
		//List<com.knife.news.model.PicType> types;
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql,paras));
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getRootPicTypes(java.lang.String, int)
	 */
	//@Override
	public List<PicType> getRootPicTypes(String sid,int display){
		return getSubPicTypesById(sid,"0",display);
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getPicTypeById(java.lang.String)
	 */
	//@Override
	public PicType getPicTypeById(String id) {
		try{
			com.knife.news.model.PicType atype = (com.knife.news.model.PicType) this.dao.get(com.knife.news.model.PicType.class, id);
			PicType btype = new PicType(atype);
			return btype;
		}catch(Exception e){
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getPicTypeByName(java.lang.String)
	 */
	//@Override
	public PicType getPicTypeByName(String name) {
		Collection<Object> paras=new ArrayList<Object>();
		paras.add(name);
		String id=(String)this.dao.uniqueResult("select id from k_pic_type where k_name=? order by length(k_parent),k_parent",paras);
		PicType btype = new PicType((com.knife.news.model.PicType) this.dao.get(com.knife.news.model.PicType.class, id));
		return btype;
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getPicTypesByTemplate(java.lang.String)
	 */
	//@Override
	public List<PicType> getPicTypesByTemplate(String template) {
		Collection<Object> paras=new ArrayList<Object>();
		paras.add(template);
		paras.add(template);
		String sql="k_pic_type_template=? or k_news_template=?";
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql,paras));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getPicTypesBySql(java.lang.String)
	 */
	//@Override
	public List<PicType> getPicTypesBySql(String sql) {
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getPicTypesBySql(java.lang.String, java.util.Collection, int, int)
	 */
	//@Override
	public List<com.knife.news.object.PicType> getPicTypesBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class, sql, paras, begin, end));
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSubPicTypesById(java.lang.String)
	 */
	//@Override
	public List<PicType> getSubPicTypesById(String parentid) {
		return this.getSubPicTypesById(parentid, -1);
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSubPicTypesById(java.lang.String, int)
	 */
	//@Override
	public List<PicType> getSubPicTypesById(String parentid,int display) {
		return this.getSubPicTypesById("",parentid, display,"","");
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSubPicTypesById(java.lang.String, java.lang.String, int)
	 */
	//@Override
	public List<PicType> getSubPicTypesById(String siteid,String parentid,int display) {
		return this.getSubPicTypesById(siteid,parentid, display,"","");
	}

	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSubPicTypesById(java.lang.String, int, java.lang.String, java.lang.String)
	 */
	//@Override
	public List<PicType> getSubPicTypesById(String parentid,int display,String userid,String tree_id) {
		return this.getSubPicTypesById("",parentid, display,userid,tree_id);
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getSubPicTypesById(java.lang.String, java.lang.String, int, java.lang.String, java.lang.String)
	 */
	//@Override
	public List<PicType> getSubPicTypesById(String siteid,String parentid,int display,String userid,String tree_id) {
		String sql = "1=1";
		Collection<Object> paras=new ArrayList<Object>();
		if(siteid.length()>0){
			paras.add(siteid);
			sql += " and k_site=?";
		}
		if(parentid.length()>0){
			if(parentid.equals("0")){
				sql += " and (k_parent='0' or k_parent='' or k_parent is null)";
			}else{
				paras.add(parentid);
				sql += " and k_parent=?";
			}
		}
		if (display>=0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(userid.length()>0){
			if(!userid.equals("0")){
				User auser=userDAO.getUserById(userid);
				if(auser!=null){
					if(auser.getPopedom()==0){
						sql += " and k_admin like '%" + userid + ",%'";
					}else if(auser.getPopedom()==1){
						sql += " and k_audit like '%" + userid + ",%'";
					}
				}
			}
		}
		if(tree_id.length()>0){
			paras.add(tree_id);
			sql += " and k_tree_id=?";
		}else{
			sql += " and (k_tree_id is null or k_tree_id='')";
		}
		sql += " order by length(k_order),k_order";
		//System.out.println("组装的SQL:"+sql);
		return changeListPicType(this.dao.query(com.knife.news.model.PicType.class,sql,paras));
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllSubPicTypesId(java.lang.String, int)
	 */
	//@Override
	public String getAllSubPicTypesId(String parentid,int display){
		return getAllSubPicTypesId("",parentid,display);
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllSubPicTypesId(java.lang.String, java.lang.String, int)
	 */
	//@Override
	public String getAllSubPicTypesId(String siteid,String parentid,int display){
		StringBuffer buf = new StringBuffer();
		//buf.append("'"+parentid+"'");
		PicType thistype=getPicTypeById(parentid);
		String sql = "id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(siteid.length()>0){
			paras.add(siteid);
			sql += " and k_site=?";
		}
		if(parentid.length()>0){
			paras.add(parentid);
			sql += " and (k_parent=?";
			sql+=")";
		}
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		List<Object> types=this.dao.query(com.knife.news.model.PicType.class, sql, paras);
		for(Object atype : types){
			com.knife.news.model.PicType aPicType=(com.knife.news.model.PicType)atype;
			buf.append(",'"+aPicType.getId()+"'");
		}
		/*
		if(this.getPicTypeById(parentid).getTree()!=null){
			List<com.knife.news.object.PicType> subTreePicTypes = this.getAllTreePicTypes(thistreeid,1);
			for(Object btype : subTreePicTypes){
				com.knife.news.object.PicType bPicType =(com.knife.news.object.PicType)btype;
				if(buf.indexOf("'"+bPicType.getId()+"'")<=0){
					buf.append(",'"+bPicType.getId()+"'");
				}
			}
		}
		List<com.knife.news.object.PicType> subPicTypes = this.getSubPicTypesById(parentid);
		if(subPicTypes!=null){
			for(Object atype : subPicTypes){
				com.knife.news.object.PicType aPicType =(com.knife.news.object.PicType)atype;
				if(aPicType.getTree()!=null){
					List<com.knife.news.object.PicType> subTreePicTypes = this.getAllTreePicTypes(aPicType.getTree(),1);
					for(Object btype : subTreePicTypes){
						com.knife.news.object.PicType bPicType =(com.knife.news.object.PicType)btype;
						if(buf.indexOf("'"+bPicType.getId()+"'")<=0){
							buf.append(",'"+bPicType.getId()+"'");
						}
					}
				}
				if(aPicType.getId()!=null){
					//subtypesid+=","+this.getAllSubPicTypesId(aPicType.getId());
					//subtypesid+=","+this.getAllSubPicTypesId(aPicType.getId());
					buf.append(","+this.getAllSubPicTypesId(aPicType.getId()));
				}
			}
		}
		//return subtypesid;
		*/
		return buf.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getAllSubPicTypesById(java.lang.String, int)
	 */
	//@Override
	public List<PicType> getAllSubPicTypesById(String cid,int display){
		String subtypeids=getAllSubPicTypesId("",cid,display);
		//System.out.println("aaaaaaaaaaa subtypes id:"+subtypeids);
		String[] ids=subtypeids.split(",");
		List<PicType> stypes=new ArrayList<PicType>();
		for(String id:ids){
			if(id.length()>0){
				id=id.replace("'", "");
				PicType atype=this.getPicTypeById(id);
				if(atype!=null){
					stypes.add(atype);
				}
			}
		}
		if(stypes.size()>0){
			return stypes;
		}else{
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.knife.news.logic.impl.PicTypeService#getRightSubPicTypesById(java.lang.String, java.lang.String)
	 */
	//@Override
	public List<PicType> getRightSubPicTypesById(String parentid, String userid) {
		return getSubPicTypesById(parentid,1,userid, "");
	}

	public boolean hasRightUser(String id, String userid,int pop) {
		if (id.equals("")) {
			id = "0";
		}
		if (userid.equals("")) {
			userid = "0";
		}
		String column="k_admin";
		if(pop==0){
			column="k_admin";
		}else if(pop==1){
			column="k_audit";
		}
		if (this.dao.uniqueResult("select * from k_pic_type where id='" + id
				+ "' and "+column+" like '%" + userid + ",%'") != null) {
			return true;
		} else {
			return false;
		}
	}

	public void reOrderPicTypes(PicType type) {
		if (type != null) {
			List<PicType> types = new LinkedList<PicType>();
			String sql = "k_parent='" + type.getParent() + "' and k_order>"
					+ type.getOrder() + " order by length(k_order),k_order";
			if (getPicTypesBySql(sql) != null) {
				types = getPicTypesBySql(sql);
			}
			for (Object atype : types) {
				//com.knife.news.model.PicType btype = new com.knife.news.model.PicType();
				PicType btype = (PicType) atype;
				//btype.setPicType((PicType)atype);
				if (btype.getOrder() > 1) {
					btype.setOrder(btype.getOrder() - 1);
				}
				updatePicType(btype);
			}
		}
	}

	public void reArrange(String siteid,PicType oldPicType, String parent,String treeid, int newOrder) {
		if (!parent.equals("")) {
			if (newOrder > 0) {
				List<PicType> types = this.getSubPicTypesById(siteid,parent,-1,"",treeid);
				int maxOrder = types.size();
				if (newOrder > maxOrder) {
					newOrder = maxOrder;
				}
				int oldOrder = oldPicType.getOrder();
				String oldSite=CommUtil.null2String(oldPicType.getSite());
				if(siteid.equals(oldPicType.getSite()) || oldSite.length()==0){
					//System.out.println("获取到的频道数:"+maxOrder);
					Collection<Object> paras = new ArrayList<Object>();
					String sql="1=1";
					if(oldSite.length()>0){
						paras.add(oldSite);
						sql+=" and k_site=?";
					}
					if(treeid.length()>0){
						paras.add(treeid);
						sql+=" and k_tree_id=?";
					}
				if (!parent.equals(oldPicType.getParent())) {
					dao.execute("update k_pic_type set k_order=k_order-1 where "+sql+" and k_parent='"+ oldPicType.getParent() +"' and k_order>"+ oldOrder,paras);
					dao.execute("update k_pic_type set k_order=k_order+1 where "+sql+" and k_parent='"+ parent +"' and k_order>="+ newOrder,paras);
				} else {
					//System.out.println("获取到的频道数:"+maxOrder);
					if (oldOrder != newOrder) {
						types = changeListPicType(dao.query(com.knife.news.model.PicType.class, sql, paras));
						maxOrder = types.size();
						if (newOrder > maxOrder) {
							newOrder = maxOrder;
						}
						if (oldOrder < newOrder) {
							
							dao
									.execute("update k_pic_type set k_order=k_order-1 where "+sql+" and k_parent='"
											+ parent
											+ "' and k_order<="
											+ newOrder
											+ " and k_order>="
											+ oldOrder,paras);
						} else {
							dao
									.execute("update k_pic_type set k_order=k_order+1 where "+sql+" and k_parent='"
											+ parent
											+ "' and k_order>="
											+ newOrder
											+ " and k_order<="
											+ oldOrder,paras);
						}
					}
				}
				oldPicType.setParent(parent);
				oldPicType.setOrder(newOrder);
				com.knife.news.model.PicType atype=new com.knife.news.model.PicType();
				atype.setPicType(oldPicType);
				dao.update(atype);
				}
			}
		}
	}
	
	public List<PicType> changeListPicType(List<Object> objs){
		List<PicType> results = new ArrayList<PicType>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.PicType robj = (com.knife.news.model.PicType) newobj;
				PicType tobj =  new PicType(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
	
	public List<PicType> md2obj(List<com.knife.news.model.PicType> types){
		List<PicType> results = new ArrayList<PicType>();
		results.clear();
		if(types!=null){
			for (Object atype : types) {
				com.knife.news.model.PicType atp = (com.knife.news.model.PicType) atype;
				PicType btype = new PicType(atp);
				results.add(btype);
			}
		}
		return results;
	}
}