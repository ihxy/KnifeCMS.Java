package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.TreeService;
import com.knife.news.model.Tree;

public class TreeServiceImpl extends DAOSupportService implements TreeService {
	private static TreeServiceImpl treeDAO = new TreeServiceImpl();

	public static TreeServiceImpl getInstance() {
		return treeDAO;
	}

	public boolean saveTree(Tree tree){
		return this.dao.save(tree);
	}

	public boolean delTree(Tree tree){
		return this.dao.del(tree);
	}

	public boolean updateTree(Tree tree){
		return this.dao.update(tree);
	}

	public Tree getTreeById(String id){
		return (Tree) this.dao.get(Tree.class, id);
	}
	
	public List<Tree> getAllTree(int isshow){
		String sql = "";
		Collection<Object> paras=new ArrayList<Object>();
		if (isshow >= 0) {
			paras.add(isshow);
			sql = "id!='' and k_show=?";
		} else {
			sql = "id!=''";
		}
		return changeListType(this.dao.query(Tree.class,sql,paras));
	}
	
	public List<Tree> getTreesBySql(String sql) {
		return changeListType(this.dao.query(Tree.class, sql));
	}

	public List<Tree> getTreesBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(Tree.class, sql, paras, begin, end));
	}
	
	public List<Tree> changeListType(List<Object> trees){
		List<Tree> results = new ArrayList<Tree>();
		if(trees!=null){
			for (Object newobj : trees) {
				Tree tree = (Tree) newobj;
				results.add(tree);
			}
		}
		return results;
	}
}