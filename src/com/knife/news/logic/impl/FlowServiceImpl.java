package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.FlowService;
import com.knife.news.object.Flow;

public class FlowServiceImpl extends DAOSupportService implements FlowService {
	private static FlowServiceImpl flowDao = new FlowServiceImpl();

	public static FlowServiceImpl getInstance() {
		return flowDao;
	}

	public boolean saveFlow(Flow flow) {
		com.knife.news.model.Flow aflow=new com.knife.news.model.Flow();
		flow.setId(aflow.getId());
		aflow.setFlow(flow);
		return this.dao.save(aflow);
	}
	
	public boolean updateFlow(Flow flow) {
		com.knife.news.model.Flow aflow=new com.knife.news.model.Flow();
		aflow.setFlow(flow);
		return this.dao.update(aflow);
	}
	
	public Flow getFlowById(String id) {
		try{
			com.knife.news.model.Flow aflow = (com.knife.news.model.Flow) this.dao.get(com.knife.news.model.Flow.class, id);
			Flow flow = new Flow(aflow);
			return flow;
		}catch(Exception e){
			return null;
		}
	}
	
	public boolean delFlow(Flow flow) {
		com.knife.news.model.Flow aflow=new com.knife.news.model.Flow();
		aflow.setFlow(flow);
		return dao.del(aflow);
	}
	
	public List<Flow> getAllFlow() {
		return changeListType(this.dao.query(com.knife.news.model.Flow.class, "id!=''"));
		//return this.dao.query(News.class, "k_type=?", paras);
	}
	
	public List<Flow> getFlowBySql(String sql){
		return changeListType(this.dao.query(com.knife.news.model.Flow.class, sql));
	}
	
	public List<Flow> getFlowBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Flow.class, sql, paras, begin, end));
	}
	
	public List<Flow> changeListType(List<Object> objs){
		List<Flow> results = new ArrayList<Flow>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Flow robj = (com.knife.news.model.Flow) newobj;
				Flow tobj =  new Flow(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}
