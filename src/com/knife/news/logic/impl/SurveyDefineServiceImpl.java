package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.SurveyDefineService;
import com.knife.news.object.SurveyDefine;

public class SurveyDefineServiceImpl extends DAOSupportService implements SurveyDefineService {
	private static SurveyDefineServiceImpl surveyDefinedao = new SurveyDefineServiceImpl();

	public static SurveyDefineServiceImpl getInstance() {
		return surveyDefinedao;
	}

	public SurveyDefine getSurveyDefine(){
		com.knife.news.model.SurveyDefine asurvey=(com.knife.news.model.SurveyDefine)this.dao.uniqueResult("select * from k_survey_define");
		return new SurveyDefine(asurvey);
	}
	
	public List<SurveyDefine> getAllSurveyDefines(){
		return changeListType(this.dao.query(com.knife.news.model.SurveyDefine.class, "id!=''"));
	}

	public List<SurveyDefine> getSurveyDefinesBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.SurveyDefine.class, sql));
	}

	public List<SurveyDefine> getSurveyDefinesBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.SurveyDefine.class, sql, paras, begin, end));
	}

	public SurveyDefine getSurveyDefineById(String id) {
		com.knife.news.model.SurveyDefine asurvey=(com.knife.news.model.SurveyDefine)this.dao.get(com.knife.news.model.SurveyDefine.class, id);
		return new SurveyDefine(asurvey);
	}

	public boolean saveSurveyDefine(SurveyDefine surveyDefine) {
		com.knife.news.model.SurveyDefine asurvey=new com.knife.news.model.SurveyDefine();
		surveyDefine.setId(asurvey.getId());
		asurvey.setSurveyDefine(surveyDefine);
		return this.dao.save(asurvey);
	}

	public boolean updateSurveyDefine(SurveyDefine surveyDefine) {
		com.knife.news.model.SurveyDefine asurvey=new com.knife.news.model.SurveyDefine();
		asurvey.setSurveyDefine(surveyDefine);
		return this.dao.update(asurvey);
	}

	public boolean delSurveyDefine(SurveyDefine surveyDefine) {
		com.knife.news.model.SurveyDefine asurvey=new com.knife.news.model.SurveyDefine();
		asurvey.setSurveyDefine(surveyDefine);
		return this.dao.del(asurvey);
	}
	
	public List<SurveyDefine> changeListType(List<Object> objs){
		List<SurveyDefine> results = new ArrayList<SurveyDefine>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.SurveyDefine robj = (com.knife.news.model.SurveyDefine) newobj;
				SurveyDefine asurvey=new SurveyDefine(robj);
				results.add(asurvey);
			}
		}
		return results;	
	}
}