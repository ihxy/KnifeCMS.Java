package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.PicType;

public interface PicTypeService {

	public abstract String savePicType(PicType type);

	public abstract boolean updatePicType(PicType type);

	public abstract boolean delPicType(PicType type);

	public abstract int getSum(int display, String parentid, String tree_id);
	
	public abstract List<PicType> getPicTypesByUser(int userid);

	public abstract List<PicType> getAllPicTypes(int display);

	public abstract List<PicType> getAllPicTypes(String siteid);

	public abstract List<PicType> getAllPicTypes(int userid, int display);

	public abstract List<PicType> getAllPicTypes(String siteid, int userid,
			int display);

	public abstract List<PicType> getAllRightPicTypes(String userid);

	public abstract List<PicType> getRootPicTypes(String sid, int display);

	public abstract PicType getPicTypeById(String id);

	public abstract PicType getPicTypeByName(String name);

	public abstract List<PicType> getPicTypesByTemplate(String template);

	public abstract List<PicType> getPicTypesBySql(String sql);

	public abstract List<com.knife.news.object.PicType> getPicTypesBySql(
			String sql, Collection<Object> paras, int begin, int end);

	public abstract List<PicType> getSubPicTypesById(String parentid);

	public abstract List<PicType> getSubPicTypesById(String parentid,
			int display);

	public abstract List<PicType> getSubPicTypesById(String siteid,
			String parentid, int display);

	public abstract List<PicType> getSubPicTypesById(String parentid,
			int display, String userid, String tree_id);

	public abstract List<PicType> getSubPicTypesById(String siteid,
			String parentid, int display, String userid, String tree_id);

	public abstract String getAllSubPicTypesId(String parentid, int display);

	public abstract String getAllSubPicTypesId(String siteid, String parentid,
			int display);

	public abstract List<PicType> getAllSubPicTypesById(String cid, int display);

	public abstract List<PicType> getRightSubPicTypesById(String parentid,
			String userid);

}