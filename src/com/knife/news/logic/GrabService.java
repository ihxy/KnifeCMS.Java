package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Grab;

/**
 * 采集接口
 */
public interface GrabService {
	/**
	 * 保存采集
	 * @param grab 采集对象
	 * @return true保存成功，false保存失败
	 */
	String saveGrab(Grab grab);

	/**
	 * 更新采集
	 * @param grab 采集对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateGrab(Grab grab);

	/**
	 * 删除采集
	 * @param grab 采集对象
	 * @return true删除成功，false删除失败
	 */
	boolean delGrab(Grab grab);

	/**
	 * 取得所有采集
	 * @return 采集列表
	 */
	List<Grab> getGrabs();
	
	/**
	 * 按条件取得采集
	 * @param sql
	 * @param paras
	 * @return 采集列表
	 */
	List<Grab> getGrabs(String sql, Collection<Object> paras);
	
	/**
	 * 按条件取得一定范围内的采集
	 * @param sql
	 * @param paras
	 * @param begin
	 * @param end
	 * @return 采集列表
	 */
    List<Grab> getGrabsBySql(String sql,Collection<Object> paras,int begin,int end);
	Grab getGrabById(String id);
}
