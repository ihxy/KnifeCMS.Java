package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Survey;

/**
 * 调查接口
 * @author Knife
 *
 */
public interface SurveyService {
	
	/**
	 * 取得所有调查结果列表
	 * @return 调查结果列表
	 */
	List<Survey> getAllSurvey();
	
	/**
	 * 按调查id取得结果列表
	 * @param sid 调查id
	 * @return 调查结果列表
	 */
	List<Survey> getSurveyBySid(String sid);
	
	/**
	 * 按条件取得调查结果列表
	 * @param sql 查询条件
	 * @return 调查结果列表
	 */
	List<Survey> getSurveyBySql(String sql);
	
	/**
	 * 按条件取得指定范围的调查结果列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 调查结果列表
	 */
	List<Survey> getSurveyBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得调查
	 * @param id 调查编号
	 * @return 调查对象
	 */
	Survey getSurveyById(String id);
	
	/**
	 * 按用户名密码取得调查
	 * @param user 调查编号
	 * @param pass 调查密码
	 * @return 调查对象
	 */
	Survey getSurveyByUserAndPass(String user,String pass);
	
	/**
	 * 保存调查
	 * @param survey 调查对象
	 * @return ture保存成功，false保存失败
	 */
	boolean saveSurvey(Survey survey);
	
	/**
	 * 更新调查
	 * @param survey 调查对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateSurvey(Survey survey);
	
	/**
	 * 删除调查
	 * @param survey 调查对象
	 * @return true删除成功，false删除失败
	 */
	boolean delSurvey(Survey survey);
}
