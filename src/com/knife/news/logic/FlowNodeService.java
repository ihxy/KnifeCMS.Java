package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.FlowNode;

public interface FlowNodeService {

	boolean saveFlowNode(FlowNode flowNode);
	
	boolean updateFlowNode(FlowNode flowNode);
	
	FlowNode getFlowNodeById(String id);
	
	boolean delFlowNode(FlowNode flowNode);
	
	List<FlowNode> getAllFlowNode();
	
	List<FlowNode> getFlowNodeByFlowId(String flowId);
	
	FlowNode getPrevFlowNode(FlowNode flowNode);
	
	FlowNode getNextFlowNode(FlowNode flowNode);
	
	List<FlowNode> getFlowNodeBySql(String sql, Collection<Object> paras, int begin, int end);
}
