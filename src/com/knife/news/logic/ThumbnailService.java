package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Thumbnail;

;

/**
 * 缩略图接口
 * @author Knife
 *
 */
public interface ThumbnailService {
	
	/**
	 * 保存缩略图
	 * @param thumb 缩略图对象
	 * @return true成功,false失败
	 */
	boolean saveThumb(Thumbnail thumb);

	/**
	 * 更新缩略图
	 * @param thumb 缩略图对象
	 * @return true成功,false失败
	 */
	boolean updateThumb(Thumbnail user);

	/**
	 * 删除缩略图
	 * @param thumb 缩略图对象
	 * @return true成功,false失败
	 */
	boolean deleteThumb(Thumbnail user);

	Thumbnail getThumbById(String id);

	Thumbnail getThumbByName(String name);

	List<Thumbnail> queryUserThumb();

	List<Thumbnail> getThumbsBySql(String sql);

	List<Thumbnail> getThumbsBySql(String sql, Collection<Object> paras, int begin, int end);
}
