package com.knife.news.logic;

import com.knife.news.model.Vote;

/**
 * 投票接口
 * @author Knife
 *
 */
public interface VoteService {
	
	/**
	 * 保存投票
	 * @param vote 投票对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveVote(Vote vote);

	/**
	 * 删除投票
	 * @param vote 投票对象
	 * @return true删除成功，false删除失败
	 */
	boolean delVote(Vote vote);

	/**
	 * 更新投票
	 * @param vote 投票对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateVote(Vote vote);

	/**
	 * 根据编号取得投票
	 * @param id 投票编号
	 * @return 投票对象
	 */
	Vote getVoteById(String id);
}