package com.knife.news.object;
/**
 * 表单字段
 * @author Knife
 *
 */
public class FormField {
	String name;
	String label;
	String order;
	String type;
	String value;
	String userValue;
	String require;

	/**
	 * 取得名称
	 * @return 名称
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 取得标签
	 * @return 标签
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * 设置标签
	 * @param label 标签
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getOrder() {
		return order;
	}
	
	/**
	 * 设置编号
	 * @param order 编号
	 */
	public void setOrder(String order) {
		this.order = order;
	}
	
	/**
	 * 取得类型
	 * @return 类型，包括文本/文本域/单选/复选等
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 设置类型
	 * @param type 类型，包括文本/文本域/单选/复选等
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 取得字段值
	 * @return 字段值
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * 设置字段值
	 * @param value 字段值
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * 取得用户定义值
	 * @return 用户定义值
	 */
	public String getUserValue() {
		return userValue;
	}
	
	/**
	 * 设置用户定义值
	 * @param userValue 用户定义值
	 */
	public void setUserValue(String userValue) {
		this.userValue = userValue;
	}
	
	/**
	 * 取得是否必填
	 * @return 是否必填:"1"必填
	 */
	public String getRequire() {
		return require;
	}
	
	/**
	 * 设置是否必填
	 * @param require 是否必填:"1"必填
	 */
	public void setRequire(String require) {
		this.require = require;
	}
}
