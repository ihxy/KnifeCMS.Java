package com.knife.news.object;

import java.util.Date;

import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.news.logic.NewsService;
import com.knife.news.logic.impl.NewsServiceImpl;

/**
 * 评论对象
 * @author Knife
 *
 */
public class Comment {

	private String id;

	private String user;
	//jlm
	private Long uid;

	private Date date;

	private String comment;

	private String news_id;
	
	private int display;
	
	private NewsService newsDAO=new NewsServiceImpl();
	
	/**
	 * 构造方法，将数据库模型转换为对象
	 * @param comm 评论的数据模型对象
	 */
	public Comment(com.knife.news.model.Comment comm){
		this.id=comm.getId();
		//jlm
		this.uid=comm.getUid();
		this.user=comm.getUser();
		this.date=comm.getDate();
		this.news_id=comm.getNews_id();
		this.comment=comm.getComment();
		this.display=comm.getDisplay();
	}

	/**
	 * 取得评论内容
	 * @return 评论内容
	 */
	public String getComment() {
		this.comment=this.comment.replaceAll("(\\[)(emo\\d+)(\\])","<img class=\"emos\" src=\"/skin/WDYYhtml/images/$2.gif\" alt=\"\" />");
		return comment;
	}

	/**
	 * 设置评论内容
	 * @param comment 评论内容
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * 取得评论日期
	 * @return 评论日期
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * 设置评论日期
	 * @param date 评论日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	//jlm
	/**
	 * */
	public Long getUid(){
		return uid;
	}
	//jlm
	public void setUid(Long uid){
		this.uid = uid;
	}
	
	/**
	 * 取得评论人
	 * @return 评论人
	 */
	public String getUser() {
		return user;
	}

	/**
	 * 设置评论人
	 * @param user 评论人
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * 取得评论编号
	 * @return 评论编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置评论编号
	 * @param id 评论编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得评论文章编号
	 * @return 文章编号
	 */
	public String getNews_id() {
		return news_id;
	}

	/**
	 * 设置评论文章编号
	 * @param news_id 文章编号
	 */
	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}

	/**
	 * 取得评论文章标题
	 * @return 文章标题
	 */
	public String getNewsTitle(){
		News anews=newsDAO.getNewsById(this.news_id);
		if(anews!=null){
			return anews.getTitle();
		}else{
			return null;
		}
	}
	
	/**
	 * 取得评论状态
	 * @return 0不显示，1显示
	 */
	public int getDisplay() {
		return display;
	}

	/**
	 * 设置评论状态
	 * @param display 评论状态
	 */
	public void setDisplay(int display) {
		this.display = display;
	}
	
	public Userinfo getCommentUser(){
		UserinfoDAO userDAO=new UserinfoDAO();
		Userinfo cuser=userDAO.findByUid(this.getUser());
		return cuser;
	}
}
