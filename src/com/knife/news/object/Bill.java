package com.knife.news.object;

import java.util.Date;
import com.knife.news.logic.SupplierService;
import com.knife.news.logic.impl.SupplierServiceImpl;

/**
 * 账单对象
 * @author Administrator
 *
 */
public class Bill {
	private String id;
	private String suser;
	private String file;
	private Date date;
	
	private SupplierService supplierDAO=SupplierServiceImpl.getInstance();
	
	public Bill(){
	}
	
	public Bill(com.knife.news.model.Bill bill){
		this.id=bill.getId();
		this.suser=bill.getSuser();
		this.file=bill.getFile();
		this.date=bill.getDate();
	}
	
	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
    
	/**
	 * 取得供应商
	 * @return 供应商编号
	 */
	public String getSuser() {
		return suser;
	}

	/**
	 * 设置供应商
	 * @param suser 供应商编号
	 */
	public void setSuser(String suser) {
		this.suser = suser;
	}

	/**
	 * 取得账单文件名
	 * @return 账单文件名
	 */
	public String getFile() {
		return file;
	}

	/**
	 * 设置账单文件名
	 * @param file 账单文件名
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * 取得账单日期
	 * @return 账单日期
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * 设置账单日期
	 * @param date 账单日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * 取得供应商名称
	 * @return 供应商名称
	 */
	public String getSname(){
		try{
			Supplier supplier=supplierDAO.getSupplierById(this.suser);
			return supplier.getSname();
		}catch(Exception e){
			return "";
		}
	}
}
