package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Type;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class TypeManageAjaxAction extends ManageAction {
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	
	public Page doAdd(WebForm form,Module module){
		Type type = (Type) form.toPo(Type.class);
		String sid=CommUtil.null2String(form.get("site"));
		if(sid.length()==0){
			sid="0";
		}
		type.setSite(sid);
		String tid=typeDAO.saveType(type);
		if (tid.length()>0) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"添加频道:"+type.getName());
			logDAO.saveLogs(log);
			form.addResult("jvalue", tid);
		} else {
			form.addResult("jvalue", "0");
		}
		return module.findPage("ajax");
	}
	
	public Page doShowList(WebForm form,Module module) {
		String uid=CommUtil.null2String(form.get("uid"));
		List<Type> types=new ArrayList<Type>();
		String checkStyle=CommUtil.null2String(form.get("checkStyle"));
		if(checkStyle.equals("")){checkStyle="checkbox";}
		if(popedom>0){
			types = typeDAO.getAllTypes(-1);
		}else{
			types=typeDAO.getAllRightTypes(user.getId());
		}
		if(uid.length()>0){
			form.addResult("uid", uid);
		}
		form.addResult("typeList", types);
		form.addResult("checkStyle", checkStyle);
		return module.findPage("dialog");
	}
	
	public Page doOrder(WebForm form,Module module) {
		String sid = CommUtil.null2String(form.get("sid"));
		String id = CommUtil.null2String(form.get("id"));
		String parent = CommUtil.null2String(form.get("parent"));
		String treeid = CommUtil.null2String(form.get("treeid"));
		int order = CommUtil.null2Int(form.get("order"));
		Type type = typeDAO.getTypeById(id);
		//System.out.println("程序要移动频道: "+type.getName()+" ，移到"+parent+"下，移动顺序是"+order);
		typeDAO.reArrange(sid,type, parent,treeid, order);
		return null;
	}
	
	//重置排序数据
	public Page doResetOrder(WebForm form, Module module){
		String sid = CommUtil.null2String(form.get("sid"));
		String parent = CommUtil.null2String(form.get("parent"));
		String sql="1=1";
		List<Object> paras=new ArrayList<Object>();
		if(sid.length()>0){
			sql+=" and k_site=?";
			paras.add(sid);
		}
		if(parent.length()<=0){
			parent="0";
		}
		sql+=" and k_parent=?";
		paras.add(parent);
		sql+="order by length(k_order),k_order";
		List<Type> types=typeDAO.getTypesBySql(sql, paras,0,-1);
		for(int i=0;i<types.size();i++){
			Type atype = types.get(i);
			atype.setOrder(i+1);
			typeDAO.updateType(atype);
		}
		return null;
	}
	
	public Page doRename(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		String name = CommUtil.null2String(form.get("name"));
		//System.out.print("获取到的名称:"+name);
		Type type = typeDAO.getTypeById(id);
		type.setName(name);
		//记录日志
		Logs log=new Logs(user.getUsername()+"编辑频道:"+type.getName());
		logDAO.saveLogs(log);
		typeDAO.updateType(type);
		return null;
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		String sid = CommUtil.null2String(form.get("sid"));
		//如果存在子频道，先删除所有子频道
		List<Type> dtypes;
		if(!id.equals("0")){
			dtypes=typeDAO.getSubTypesById(id);
		}else{
			dtypes=typeDAO.getAllTypes(sid, "", -1);
		}
		for(Type atype:dtypes){
			//记录日志
			Logs log=new Logs(user.getUsername()+"删除频道:"+atype.getName());
			logDAO.saveLogs(log);
			typeDAO.delType(atype);
		}
		if(!id.equals("0")){
			Type type = typeDAO.getTypeById(id);
			//记录日志
			Logs log=new Logs(user.getUsername()+"删除频道:"+type.getName());
			logDAO.saveLogs(log);
			typeDAO.delType(type);
		}
		return null;
	}
	
	public Page doSetTypeDisplay(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubTypeDisplay(type);
		return null;
	}
	
	public Page doSetIndexTemplate(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubIndexTemplate(type);
		return null;
	}
	
	public Page doSetTypeTemplate(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubTypeTemplate(type);
		return null;
	}
	
	public Page doSetNewsTemplate(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubNewsTemplate(type);
		return null;
	}
	
	public Page doSetSubAdmin(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubAdmin(type);
		return null;
	}
	
	public Page doSetSubAudit(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubAudit(type);
		return null;
	}
	
	public Page doSetSubFlow(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubFlow(type);
	
		return null;
	}
	
	public Page doSetSubForm(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		Type type = typeDAO.getTypeById(id);
		setSubForm(type);
		return null;
	}
	
	private void setSubTypeDisplay(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setDisplay(type.getDisplay());
			typeDAO.updateType(atype);
			setSubTypeDisplay(atype);
		}
	}
	
	private void setSubIndexTemplate(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setIndex_template(type.getIndex_template());
			typeDAO.updateType(atype);
			setSubIndexTemplate(atype);
		}
	}
	
	private void setSubTypeTemplate(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setType_template(type.getType_template());
			typeDAO.updateType(atype);
			setSubTypeTemplate(atype);
		}
	}
	
	private void setSubNewsTemplate(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setNews_template(type.getNews_template());
			typeDAO.updateType(atype);
			setSubNewsTemplate(atype);
		}
	}
	
	private void setSubAdmin(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setAdmin(type.getAdmin());
			typeDAO.updateType(atype);
			setSubAdmin(atype);
		}
	}
	
	private void setSubAudit(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setAudit(type.getAudit());
			typeDAO.updateType(atype);
			setSubAudit(atype);
		}
	}
	
	private void setSubFlow(Type type){
		List<Type> subtypes = typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setFlow(type.getFlow());
			typeDAO.updateType(atype);
			setSubFlow(atype);
		}
	
	}
	private void setSubForm(Type type){
		List<Type> subtypes=typeDAO.getSubTypesById(type.getId());
		for(Type atype:subtypes){
			atype.setForm(type.getForm());
			typeDAO.updateType(atype);
			setSubForm(atype);
		}
	}
}
