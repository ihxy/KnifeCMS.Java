package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;


import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.news.logic.OrderService;
import com.knife.news.logic.impl.OrderServiceImpl;
import com.knife.news.object.Order;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class OrderManageAction extends ManageAction {

	OrderService orderDAO = OrderServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Order> allfilts = orderDAO.getAllOrders();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Order> firstfilts = orderDAO.getOrdersBySql("id!=''", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("orderList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Order> allfilts = orderDAO.getAllOrders();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Order> filterList = new ArrayList<Order>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			filterList = orderDAO
					.getOrdersBySql("id!=''", null, begin - 1, end);
		} else {
			filterList = orderDAO.getOrdersBySql("id!=''", null, begin - 1,
					pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("orderList", filterList);
		return module.findPage("list");
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Order filter = orderDAO.getOrderById(id);
		form.addResult("filter", filter);
		form.addResult("action", "update");
		return module.findPage("edit");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result=false;
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Order order = orderDAO.getOrderById(ids[i]);
				result = orderDAO.delOrder(order);
			}
		} else {
			Order order = orderDAO.getOrderById(id);
			result = orderDAO.delOrder(order);
		}
		if(result){
			form.addResult("msg", "删除成功");
		}else{
			form.addResult("msg", "删除失败");
		}
		return doList(form, module);
	}
	
	public Page doConfirm(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		Order order = orderDAO.getOrderById(id);
		UserinfoDAO uDAO=new UserinfoDAO();
		try{
			Userinfo ouser=(Userinfo)uDAO.findActiveByAcount(order.getUser()).get(0);
			if(ouser.getScore()>=order.getMoney()){
				ouser.setScore(ouser.getScore()-order.getMoney());
				uDAO.save(ouser);
				order.setStatus(1);
				orderDAO.updateOrder(order);
				form.addResult("msg", "确认成功！");
			}else{
				form.addResult("msg", "确认失败！积分不够");
			}
		}catch(Exception e){
			e.printStackTrace();
			form.addResult("msg", "确认失败！");
		}
		return doList(form, module);
	}

	public Page doSave(WebForm form, Module module) {
		Order order = (Order) form.toPo(Order.class);
		if (orderDAO.saveOrder(order)) {
			form.addResult("msg", "添加成功！");
		} else {
			form.addResult("msg", "添加失败！");
		}
		return doList(form, module);
	}

	public Page doUpdate(WebForm form, Module module) {
		Order order = (Order) form.toPo(Order.class);
		if (orderDAO.updateOrder(order)) {
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		return doList(form, module);
	}
}
