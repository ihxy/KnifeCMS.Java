package com.knife.news.manage.action;

import java.util.List;

import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.object.User;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class UserManageAjaxAction extends ManageAction {
	private UserServiceImpl userDAO = UserServiceImpl.getInstance();
	
	public Page doInit(WebForm form,Module module){
		return doChoose(form, module);
	}
	
	public Page doAdd(WebForm form,Module module){
		return module.findPage("ajax");
	}
	
	public Page doChoose(WebForm form,Module module){
		int popedom = CommUtil.null2Int(form.get("popedom")); 
		List<User> allUsers = userDAO.getUsersBySql("k_popedom="+popedom);
		form.addResult("userList", allUsers);
		return module.findPage("dialog");
	}
}
