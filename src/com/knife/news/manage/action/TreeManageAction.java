package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.TreeService;
import com.knife.news.logic.impl.TreeServiceImpl;
import com.knife.news.model.Tree;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class TreeManageAction extends ManageAction {
	private TreeService treeDAO = TreeServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Tree> treeList = treeDAO.getAllTree(1);
		int rows = treeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Tree> firstTrees = new ArrayList<Tree>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		firstTrees = treeDAO.getTreesBySql("id!=''", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("treeList", firstTrees);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Tree> treeList = treeDAO.getAllTree(1);
		int rows = treeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Tree> firstTrees = new ArrayList<Tree>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			firstTrees = treeDAO.getTreesBySql("id!=''", null, begin - 1, end);
		} else {
			firstTrees = treeDAO.getTreesBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("treeList", firstTrees);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		Tree aTree=null;
		String id = CommUtil.null2String(form.get("id"));
		if(id.equals("")){id="0";}
		if(!id.equals("0")){
			aTree = treeDAO.getTreeById(id);
		}
		form.addResult("action", "update");
		form.addResult("tree", aTree);
		return module.findPage("edit");
	}
	
	public Page doEditType(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		form.addResult("id", id);
		return module.findPage("editType");
	}
	
	public Page doSave(WebForm form, Module module) {
		Tree tree = (Tree) form.toPo(Tree.class);
		if (treeDAO.saveTree(tree)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doUpdate(WebForm form, Module module) {
		Tree tree = (Tree) form.toPo(Tree.class);
		if (treeDAO.updateTree(tree)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", tree.getId());
			return doEdit(form, module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", tree.getId());
			return doEdit(form, module);
		}
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Tree tree = treeDAO.getTreeById(ids[i]);
				result = treeDAO.delTree(tree);
			}
		}else{
			Tree tree = treeDAO.getTreeById(id);
			result = treeDAO.delTree(tree);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
}
