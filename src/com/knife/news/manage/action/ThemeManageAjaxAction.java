package com.knife.news.manage.action;

import java.io.File;
import java.util.List;

import com.knife.news.logic.SiteService;
import com.knife.news.logic.ThemeService;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.ThemeServiceImpl;
import com.knife.news.object.Site;
import com.knife.news.object.Theme;
import com.knife.tools.CopyDirectory;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class ThemeManageAjaxAction extends ManageAction {
	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private ThemeService themeDAO = ThemeServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doChoose(form, module);
	}

	public Page doChoose(WebForm form, Module module) {
		List<Theme> tList = themeDAO.getAllTheme();
		if (tList != null) {
			form.addResult("tList", tList);
		}
		return module.findPage("dialog");
	}
	
	public Page doApply(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		String sid = CommUtil.null2String(form.get("sid"));
		Site site = siteDAO.getSite();
		if(sid.length()>0){
			site = siteDAO.getSiteById(sid);
		}
		Theme theme = themeDAO.getThemeById(id);
		//生成样式文件
		String css="x.css";
		try{
			if(site.getCss_file()!=null){
				if(site.getCss_file().length()>0){
					css=site.getCss_file();
				}
			}
			String cssPath=Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + File.separator + "web" + File.separator+site.getPub_dir()+File.separator+"skin"+File.separator+"css"+File.separator+css;
			File cssfile=new File(cssPath);
			if(cssfile.exists()){
				cssfile.delete();
			}
			cssfile.createNewFile();
			if(cssfile.canWrite()){
				org.apache.commons.io.FileUtils.writeStringToFile(cssfile,theme.getCss(),"UTF-8");
			}
			if(site.getHtml()==0){
				cssPath=Globals.APP_BASE_DIR+File.separator+"skin"+File.separator+site.getPub_dir()+File.separator+"css"+File.separator+css;
			}else if(site.getHtml()==1){
				cssPath=Globals.APP_BASE_DIR+File.separator+"html"+File.separator+site.getPub_dir()+File.separator+"skin"+File.separator+"css"+File.separator+css;
			}
			File csspubfile=new File(cssPath);
			if(csspubfile.exists()){
				csspubfile.delete();
			}
			if(!csspubfile.getParentFile().exists()){
				csspubfile.getParentFile().mkdirs();
			}
			csspubfile.createNewFile();
			if(csspubfile.canWrite()){
				org.apache.commons.io.FileUtils.writeStringToFile(csspubfile,theme.getCss(),"UTF-8");
			}
			if(site.getHtml()==1){
				CopyDirectory.copyFile(csspubfile, new File(csspubfile.getAbsolutePath().replace("html", "wwwroot")));
				if(site.getBig5()==1){
					String cssbig5path=Globals.APP_BASE_DIR+File.separator+"html"+File.separator+site.getPub_dir()+File.separator+"big5"+File.separator+"skin"+File.separator+"css"+File.separator+css;
					File cssbig5file=new File(cssbig5path);
					if(cssbig5file.exists()){
						cssbig5file.delete();
					}
					if(!cssbig5file.getParentFile().exists()){
						cssbig5file.getParentFile().mkdirs();
					}
					cssbig5file.createNewFile();
					if(cssbig5file.canWrite()){
						org.apache.commons.io.FileUtils.writeStringToFile(cssbig5file,theme.getCss(),"UTF-8");
					}
					CopyDirectory.copyFile(cssbig5file, new File(cssbig5file.getAbsolutePath().replace("html", "wwwroot")));
				}
			}
		}catch(Exception e){e.printStackTrace();}
		return null;
	}
}