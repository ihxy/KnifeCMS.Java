package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.SiteService;
import com.knife.news.logic.PicTypeService;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.PicTypeServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Site;
import com.knife.news.object.PicType;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class PicTypeManageAction extends ManageAction {
	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private PicTypeService typeDAO = PicTypeServiceImpl.getInstance();
	private String id = "";
	//private NewsService newsDAO = NewsServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		int uid=CommUtil.null2Int(form.get("uid"));
		String site_id = CommUtil.null2String(form.get("sid"));
		List<PicType> typeList = typeDAO.getAllPicTypes(site_id,uid,-1);
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<PicType> firstPicTypes = new ArrayList<PicType>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		String sql="id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(site_id.length()>0){
			sql+=" and k_site=?";
			paras.add(site_id);
			form.addResult("sid", site_id);
		}
		if(uid>0){
			sql+=" and k_userid=?";
			paras.add(uid);
			form.addResult("uid", uid);
		}
		sql+=" order by length(k_order),k_order";
		firstPicTypes = typeDAO.getPicTypesBySql(sql, paras,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("typeList", firstPicTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		int uid=CommUtil.null2Int(form.get("uid"));
		String site_id = CommUtil.null2String(form.get("sid"));
		List<PicType> typeList = typeDAO.getAllPicTypes(site_id,uid,-1);
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<PicType> firstPicTypes = new ArrayList<PicType>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(site_id.length()>0){
			sql+=" and k_site=?";
			paras.add(site_id);
			form.addResult("sid", site_id);
		}
		if(uid>0){
			sql+=" and k_userid=?";
			paras.add(uid);
			form.addResult("uid", uid);
		}
		sql+=" order by length(k_order),k_order";
		if (end < pageSize) {
			firstPicTypes = typeDAO.getPicTypesBySql(sql, paras, begin - 1, end);
		} else {
			firstPicTypes = typeDAO.getPicTypesBySql(sql, paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("typeList", firstPicTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		String pid = CommUtil.null2String(form.get("pid"));
		if(pid.equals("")){pid="0";}
		if(!pid.equals("0")){
			PicType aPicType = typeDAO.getPicTypeById(pid);
			pid = aPicType.getParent();
		}
		String site_id = CommUtil.null2String(form.get("sid"));
		if(site_id.length()>0){
			form.addResult("sid", site_id);
			Site site=siteDAO.getSiteById(site_id);
			form.addResult("site", site);
		}
		String tree_id = CommUtil.null2String(form.get("tree_id"));
		if(tree_id.length()>0){
			form.addResult("tree_id", tree_id);
		}
		List<PicType> typeList = typeDAO.getSubPicTypesById(pid);
		form.addResult("action", "save");
		form.addResult("typeList", typeList);
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		PicType aPicType=new PicType();
		Site site=new Site();
		String nid = CommUtil.null2String(form.get("id"));
		if(!nid.equals("")){id=nid;}
		if(!id.equals("")){
			aPicType = typeDAO.getPicTypeById(id);
			site=siteDAO.getSiteById(aPicType.getSite());
			//System.out.println("get siteid:"+aPicType.getSite()+",find site"+site.getName());
		}
		form.addResult("action", "update");
		form.addResult("thisSite", site);
		form.addResult("thisPicType", aPicType);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		//com.knife.news.model.PicType type = (com.knife.news.model.PicType) form.toPo(com.knife.news.model.PicType.class);
		PicType type = (PicType) form.toPo(PicType.class);
		String tid=typeDAO.savePicType(type);
		if (tid.length()>0) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"添加频道:"+type.getName());
			logDAO.saveLogs(log);
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doUpdate(WebForm form, Module module) {
		PicType type = (PicType) form.toPo(PicType.class);
		//Site site=new Site();
		if (typeDAO.updatePicType(type)) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"编辑频道:"+type.getName());
			logDAO.saveLogs(log);
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		id=type.getId();
		return doEdit(form, module);
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		//System.out.print("要删除的ID是:"+id);
		boolean result = false;
		//com.knife.news.model.PicType atype = new com.knife.news.model.PicType();
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				PicType type = typeDAO.getPicTypeById(ids[i]);
				//typeDAO.reOrderPicTypes(type);
				//记录日志
				Logs log=new Logs(user.getUsername()+"删除频道:"+type.getName());
				logDAO.saveLogs(log);
				result = typeDAO.delPicType(type);
			}
		}else{
			PicType type = typeDAO.getPicTypeById(id);
			//typeDAO.reOrderPicTypes(type);
			//记录日志
			Logs log=new Logs(user.getUsername()+"删除频道:"+type.getName());
			logDAO.saveLogs(log);
			result = typeDAO.delPicType(type);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
		} else {
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
}
