package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import tools.CalcDate;

import com.knife.news.logic.NewsService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.UserService;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.logic.impl.UserServiceImpl;
import com.knife.news.object.User;
import com.knife.news.object.PostInfo;
import com.knife.news.object.Site;
import com.knife.news.object.Type;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class PostManageAction extends ManageAction {
	SiteService siteDAO = SiteServiceImpl.getInstance();
	TypeService typeDAO = TypeServiceImpl.getInstance();
	NewsService newsDAO = NewsServiceImpl.getInstance();
	UserService userDAO = UserServiceImpl.getInstance();
	private ArrayList<PostInfo> infos = new ArrayList<PostInfo>();

	public Page doInit(WebForm form, Module module) {
		// TODO Auto-generated method stub
		return module.findPage("index");
		// return doList(form, module);
	}

	public Page doSummary(WebForm form, Module module) {
		int totalTypeNums = typeDAO.getSum(-1, "", "");
		int totalNewsNums = newsDAO.getAllSum(-1, "", "", "", "", "");
		int totalUserNums = userDAO.getSum(-1);
		form.addResult("totalTypeNums", totalTypeNums);
		form.addResult("totalNewsNums", totalNewsNums);
		form.addResult("totalUserNums", totalUserNums);
		// UserinfoDAO userinfoDAO=new UserinfoDAO();
		// int totalMemberNums=userinfoDAO.findAll().size();
		// form.addResult("totalMemberNums", totalMemberNums);
		return module.findPage("summary");
	}

	public Page doUser(WebForm form, Module module) {
		String startdate = CommUtil.null2String(form.get("startdate"));
		String enddate = CommUtil.null2String(form.get("enddate"));
		List<User> allUsers = userDAO.getAllUsers();
		for (int i = 0; i < allUsers.size(); i++) {
			User auser = allUsers.get(i);
			PostInfo ainfo = new PostInfo();
			ainfo.setName(auser.getUsername());
			ainfo.setNums(newsDAO.getSum(-1, auser.getUsername(), "", "", "",
					""));
			CalcDate.getInstance().calcLastMonth();
			// System.out.println("计算出的时间是:"+CalcDate.getInstance().getBegin()+"::::"+CalcDate.getInstance().getEnd());
			ainfo.setLastMonthNum(newsDAO.getAllSum(-1, auser.getUsername(),
					"", "", CalcDate.getInstance().getBegin(), CalcDate
							.getInstance().getEnd()));
			CalcDate.getInstance().calcThisMonth();
			ainfo.setMonthNum(newsDAO.getAllSum(-1, auser.getUsername(), "",
					"", CalcDate.getInstance().getBegin(), CalcDate
							.getInstance().getEnd()));
			ainfo.setSearchNum(newsDAO.getAllSum(-1, auser.getUsername(), "",
					"", startdate, enddate));
			infos.add(ainfo);
		}
		form.addResult("infos", infos);
		return module.findPage("list");
	}

	public Page doType(WebForm form, Module module) {
		String startdate = CommUtil.null2String(form.get("startdate"));
		String enddate = CommUtil.null2String(form.get("enddate"));
		String sid = CommUtil.null2String(form.get("sid"));
		String tid = CommUtil.null2String(form.get("tid"));
		if ("".equals(tid)) {
			tid = "0";
		}
		if ("".equals(sid)) {
			List<Site> allSites = siteDAO.getAllSites();
			for (int i = 0; i < allSites.size(); i++) {
				Site asite = allSites.get(i);
				int typenums = newsDAO.getSiteSum(-1, asite.getId(), "", "");
				PostInfo ainfo = new PostInfo();
				ainfo.setName(asite.getName());
				ainfo.setNums(typenums);
				CalcDate.getInstance().calcLastMonth();
				ainfo.setLastMonthNum(newsDAO.getSiteSum(-1, asite.getId(),
						CalcDate.getInstance().getBegin(), CalcDate
								.getInstance().getEnd()));
				CalcDate.getInstance().calcThisMonth();
				ainfo.setMonthNum(newsDAO.getSiteSum(-1, asite.getId(),
						CalcDate.getInstance().getBegin(), CalcDate
								.getInstance().getEnd()));
				ainfo.setSearchNum(newsDAO.getSiteSum(-1, asite.getId(),
						startdate, enddate));
				ainfo.setDetail(true);
				ainfo.setDetailUrl("?parameter=type&sid=" + asite.getId());
				infos.add(ainfo);
			}
		} else {
			List<Type> allTypes = typeDAO.getSubTypesById(sid,tid,-1);
			for (int i = 0; i < allTypes.size(); i++) {
				Type atype = allTypes.get(i);
				int typenums = newsDAO
						.getSum(-1, "", atype.getId(), "", "", "");
				if (typenums > 0) {
					PostInfo ainfo = new PostInfo();
					ainfo.setName(atype.getName());
					ainfo.setNums(typenums);
					CalcDate.getInstance().calcLastMonth();
					ainfo.setLastMonthNum(newsDAO.getAllSum(-1, "", atype
							.getId(), "", CalcDate.getInstance().getBegin(),
							CalcDate.getInstance().getEnd()));
					CalcDate.getInstance().calcThisMonth();
					ainfo.setMonthNum(newsDAO.getAllSum(-1, "", atype.getId(),
							"", CalcDate.getInstance().getBegin(), CalcDate
									.getInstance().getEnd()));
					ainfo.setSearchNum(newsDAO.getAllSum(-1, "", atype.getId(),
							"", startdate, enddate));
					if (!atype.isLeave()) {
						ainfo.setDetail(true);
						ainfo.setDetailUrl("?parameter=type&tid="
								+ atype.getId());
					}
					infos.add(ainfo);
				}
			}
		}
		form.addResult("infos", infos);
		return module.findPage("list");
	}

	public Page doDate(WebForm form, Module module) {

		form.addResult("infos", infos);
		return module.findPage("list");
	}
}
