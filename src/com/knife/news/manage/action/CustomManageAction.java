package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.knife.news.logic.impl.CustomServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Custom;
import com.knife.news.object.Site;
import com.knife.tools.ZipUtil;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class CustomManageAction extends ManageAction {
	private CustomServiceImpl customDAO = CustomServiceImpl.getInstance();
	private SiteServiceImpl siteDAO = SiteServiceImpl.getInstance();
	private ZipUtil z=new ZipUtil();
	private String sid="";
	
	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module){
		if(sid.equals("")){
			sid = CommUtil.null2String(form.get("sid"));
		}
		List<Custom> allCustoms = customDAO.getAllCustom();
		int rows = allCustoms.size();// 总数
		int pageSize = 18;// 每页18条
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = currentPage+1;
		List<Custom> firstCustoms = new ArrayList<Custom>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));// 计算页数
		Collection<Object> paras = new ArrayList<Object>();
		if(sid.length()>0){
			paras.add(sid);
			form.addResult("sid", sid);
			firstCustoms = customDAO.getCustomBySql("k_site=?",paras,0,pageSize);
		}else{
			firstCustoms = customDAO.getCustomBySql("1=1",paras,0,pageSize);
		}
		Site thisSite = siteDAO.getSiteById(sid);
		form.addResult("thisSite", thisSite);
		form.addResult("rows", rows);
		form.addResult("currentPage", currentPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("customList", firstCustoms);
		return module.findPage("list");
	}
	
	public Page doPage(WebForm form, Module module) {
		if(sid.equals("")){
			sid = CommUtil.null2String(form.get("sid"));
		}
		List<Custom> allCustoms = customDAO.getAllCustom();
		int rows = allCustoms.size();
		int pageSize = 18;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		Collection<Object> paras = new ArrayList<Object>();
		List<Custom> customList = new ArrayList<Custom>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="";
		if(sid.length()>0){
			paras.add(sid);
			form.addResult("sid", sid);
			sql="k_site=?";
		}else{
			sql="1=1";
		}
		if (end < pageSize) {
			customList = customDAO.getCustomBySql(sql, paras, begin - 1, end);
		} else {
			customList = customDAO.getCustomBySql(sql, paras, begin - 1, pageSize);
		}
		Site thisSite = siteDAO.getSiteById(sid);
		form.addResult("thisSite", thisSite);
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("customList", customList);
		return module.findPage("list");
	}
	
	public Page doDelete(WebForm form, Module module) {
		boolean ret=false;
		String id = CommUtil.null2String(form.get("id"));
		if(!id.equals("")){
			Custom acustom =null;
			if(id.indexOf(",")>0){
				String[] ids = id.split(",");
				for (int i = 0; i < ids.length; i++) {
					if(!ids[i].isEmpty()){
						acustom = customDAO.getCustomById(ids[i]);
						if(acustom.getFile()!=null){
							//删除解压的文件夹
							String dirName=acustom.getFile().getName();
							dirName=dirName.substring(0,dirName.indexOf("."));
							File unzipDir=new File(acustom.getFile().getParentFile().getAbsolutePath()+File.separator+dirName);
							if(unzipDir.exists()){
								z.deleteFile(unzipDir);
							}
							acustom.getFile().delete();
							File pubFile=new File(acustom.getFile().getAbsolutePath().replace("html", "wwwroot"));
							if(pubFile.exists()){
								File unzippubDir=new File(pubFile.getParentFile().getAbsolutePath()+File.separator+dirName);
								if(unzippubDir.exists()){
									z.deleteFile(unzippubDir);
								}
								pubFile.delete();
							}
						}
						ret=customDAO.delCustom(acustom);
					}
				}
			}else{
				acustom = customDAO.getCustomById(id);
				if(acustom.getFile()!=null){
					String dirName=acustom.getFile().getName();
					dirName=dirName.substring(0,dirName.indexOf("."));
					File unzipDir=new File(acustom.getFile().getParentFile().getAbsolutePath()+File.separator+dirName);
					if(unzipDir.exists()){
						z.deleteFile(unzipDir);
					}
					acustom.getFile().delete();
					File pubFile=new File(acustom.getFile().getAbsolutePath().replace("html", "wwwroot"));
					if(pubFile.exists()){
						File unzippubDir=new File(pubFile.getParentFile().getAbsolutePath()+File.separator+dirName);
						if(unzippubDir.exists()){
							z.deleteFile(unzippubDir);
						}
						pubFile.delete();
					}
				}
				//System.out.println("删除图片:"+filepath);
				ret=customDAO.delCustom(acustom);
			}
		}
		if(ret){
			form.addResult("msg", "删除成功！");
		}else{
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
	
	public Page doAdd(WebForm form, Module module) {
		String sid = CommUtil.null2String(form.get("sid"));
		form.addResult("sid", sid);
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Custom custom = customDAO.getCustomById(id);
		form.addResult("sid", custom.getSite());
		form.addResult("custom", custom);
		form.addResult("action", "update");
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		Custom custom = (Custom) form.toPo(Custom.class);
		custom.setDate(new Date());
		if (customDAO.saveCustom(custom)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}

	public Page doUpdate(WebForm form, Module module) {
		Custom custom = (Custom) form.toPo(Custom.class);
		if(custom.getDate()==null){
			custom.setDate(new Date());
		}
		if (customDAO.updateCustom(custom)) {
			form.addResult("msg", "编辑成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}
}