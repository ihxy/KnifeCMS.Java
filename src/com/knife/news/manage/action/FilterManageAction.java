package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.FiltServiceImpl;
import com.knife.news.model.Filter;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FilterManageAction extends ManageAction {

	FiltServiceImpl filtdao = FiltServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Filter> allfilts = filtdao.getAllFilters();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Filter> firstfilts = filtdao.getFiltersBySql("id!=''", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("filterList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Filter> allfilts = filtdao.getAllFilters();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Filter> filterList = new ArrayList<Filter>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			filterList = filtdao
					.getFiltersBySql("id!=''", null, begin - 1, end);
		} else {
			filterList = filtdao.getFiltersBySql("id!=''", null, begin - 1,
					pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("filterList", filterList);
		return module.findPage("list");
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Filter filter = filtdao.getFilterById(id);
		form.addResult("filter", filter);
		form.addResult("action", "update");
		return module.findPage("edit");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Filter filter = filtdao.getFilterById(id);
		filtdao.delFilter(filter);
		form.addResult("msg", "删除成功");
		return doList(form, module);
	}

	public Page doSave(WebForm form, Module module) {
		Filter filter = (Filter) form.toPo(Filter.class);
		if (filtdao.saveFilt(filter)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}

	public Page doUpdate(WebForm form, Module module) {
		Filter filter = (Filter) form.toPo(Filter.class);
		if (filtdao.updateFilt(filter)) {
			form.addResult("msg", "编辑成功！");
			return doList(form, module);
		} else {
			return doList(form, module);
		}
	}
}
