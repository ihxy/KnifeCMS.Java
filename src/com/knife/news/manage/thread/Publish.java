package com.knife.news.manage.thread;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;

import com.knife.news.logic.NewsService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Type;
import com.knife.service.HTMLGenerater;
import com.knife.util.CommUtil;
import com.knife.web.Globals;

public class Publish extends Thread{
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	private NewsService newsDAO = NewsServiceImpl.getInstance();
	Date nowDate = new Date();
	HTMLGenerater g;
	Collection<Object> paras = new ArrayList<Object>();
	Collection<Object> paras1 = new ArrayList<Object>();
	List<News> pubNews = new ArrayList<News>();
	List<News> unPubNews = new ArrayList<News>();
	List<Type> pubType = new ArrayList<Type>();
	//private Lock alock;
	private int priority=1;
	volatile boolean stop=false;
	private File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");

    public Publish(Lock mylock,int apriority) {
    	super("publish");
    	priority=apriority;
    	this.setPriority(priority);
    	//alock=mylock;
    	if(!lock.exists()){
    		try{
    		lock.createNewFile();
    		}catch(Exception e){}
    	}
    }
    
    public void reset(){
    	stop=false;
    }
    
    public void run() {
        System.out.println("KnifeCMS 发布线程开始!");
		//alock.lock();
		while(!stop){
		try {
			nowDate = new Date();
			// 发布文章
			paras.clear();
			paras.add(nowDate);
			pubNews = newsDAO.getNews("k_display='1' and k_ispub='0' and k_date<=?",paras);
			System.out.println("news will pub:"+pubNews.size());
			for (News anews : pubNews) {
				anews.pub();
			}
			// 下线文章
			unPubNews = newsDAO.getNews("k_display='1' and k_ispub='1' and k_offdate<=?", paras);
			System.out.println("news will unpub:"+unPubNews.size());
			for (News anews : unPubNews) {
				anews.unPub();
			}
			// 需要更新的频道
			pubType=typeDAO.getTypesBySql("k_needpub='1' and k_html='1'");
			System.out.println("type will update:"+pubType.size());
			for(Type atype: pubType){
				//gtype=typeDAO.getTypeById(anews.getType());
				g=new HTMLGenerater(atype.getSite());
				int rows = newsDAO.getSum(1, "",atype.getSite(),atype.getId(), "", "", "",1);
				if(rows>0){
				int pageSize = CommUtil.null2Int(atype.getPagenum());// 每页20条
				int totalPage = (int) Math.ceil((float) (rows)
						/ (float) (pageSize));// 计算页数
				for(int i=0;i<totalPage;i++){
					int nowPage=i+1;
					paras1.clear();
					paras1.add(atype.getId());
					g.saveTypeListToHtml(atype.getId(),"k_display='1' and k_ispub='1' and k_type=? order by length(k_order) desc,k_order desc", paras1,nowPage,rows);
					//System.out.println("generate:"+atype.getName()+":"+atype.getHref(nowPage));
					if(stop){break;}
				}
				}else{
					g.saveTypeIndexToHtml(atype.getId(),"");
				}
				atype.setNeedpub(0);
				typeDAO.updateType(atype);
				System.out.println("generate:"+atype.getName());
				if(stop){break;}
			}		
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			stop=true;
			if(lock.exists()){
				lock.delete();
			}
		}
		}
		//alock.unlock();
        System.out.println("KnifeCMS 发布线程结束!");
    }
}
