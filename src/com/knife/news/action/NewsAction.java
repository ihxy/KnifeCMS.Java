package com.knife.news.action;

import java.io.File;
import java.io.IOException;

import com.knife.util.CommUtil;
import com.knife.web.ActionContext;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

/**
 * 处理文章请求
 * @author Knife
 *
 */
public class NewsAction extends BaseCmdAction {

	public Page doInit(WebForm form, Module module) {
		if(count>0){
			thisNews.setCount(count);
			newsDAO.updateNews(thisNews);
		}
		return doShow(form,module);
	}
	
	public Page doShow(WebForm form, Module module){
		String useTemplate="news.htm";
		if(thisType!=null){
			if(thisType.getNews_template()!=null){
				useTemplate=thisType.getNews_template();
			}
		}
		if(useTemplate.indexOf(",")>0){
			String[] typeTemplates=useTemplate.split(",");
			int showType = CommUtil.null2Int(form.get("st"));
			useTemplate=typeTemplates[showType];
		}
		String ext="";
		try{
			ext = useTemplate.substring(useTemplate.lastIndexOf(".")+1);
		}catch(Exception e){}
		if(!ext.equalsIgnoreCase("url") && !ext.equalsIgnoreCase("json")&& !ext.equalsIgnoreCase("xml")){
			ext="html";
		}
		String siteTpl="default";
		if(thisSite!=null){
		if(thisSite.getTemplate()!=null){
			siteTpl=thisSite.getTemplate();
		}
		}
		return new Page("typeNews", useTemplate ,ext,"/web/" + siteTpl + "/");
	}
	
	public Page doPreview(WebForm form, Module module){
		File realTemplate=new File(Globals.APP_BASE_DIR + "WEB-INF/templates/web/" + thisSite.getTemplate() + "/" + thisType.getNews_template());
		File tempTemplate=new File(Globals.APP_BASE_DIR + "WEB-INF/templates/tmp/" + thisSite.getTemplate() + "/" + thisType.getNews_template());
		if(!tempTemplate.getParentFile().exists()){
			tempTemplate.getParentFile().mkdirs();
		}
		String content="";
		try {
			File[] incfiles = (new File(Globals.APP_BASE_DIR + "WEB-INF/templates/web/" + thisSite.getTemplate() + "/inc/")).listFiles();
			for (int i = 0; i < incfiles.length; i++) {
				File realincfile=incfiles[i];
				if (realincfile.isFile()) {
					File newincfile= new File(realincfile.getAbsolutePath().replace(File.separator+"web"+File.separator, File.separator+"tmp"+File.separator));
					if(!newincfile.getParentFile().exists()){
						newincfile.getParentFile().mkdirs();
					}
					if(!newincfile.exists()){
						newincfile.createNewFile();
					}
					if(newincfile.canWrite()){
						content = org.apache.commons.io.FileUtils.readFileToString(realincfile, "UTF-8");
						if(thisSite.getHtml()==1){
							content = content.replace("\"/skin/", "\"/html/"+thisSite.getPub_dir()+"/skin/");
							content = content.replace("'/skin/", "'/html/"+thisSite.getPub_dir()+"/skin/");
						}
						org.apache.commons.io.FileUtils.writeStringToFile(newincfile,content,"UTF-8");
					}
				}
			}
			if(!tempTemplate.exists()){
				tempTemplate.createNewFile();
			}
			if(tempTemplate.canWrite()){
				content = org.apache.commons.io.FileUtils.readFileToString(realTemplate, "UTF-8");
				if(thisSite.getHtml()==1){
					content = content.replace("\"/skin/", "\"/html/"+thisSite.getPub_dir()+"/skin/");
					content = content.replace("'/skin/", "'/html/"+thisSite.getPub_dir()+"/skin/");
				}
				org.apache.commons.io.FileUtils.writeStringToFile(tempTemplate,content,"UTF-8");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Page("typeNews", thisType.getNews_template(),"html","/tmp/" + thisSite.getTemplate() + "/");
	}
}