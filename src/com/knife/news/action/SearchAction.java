package com.knife.news.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jsoup.Jsoup;

import com.knife.tools.HtmlInputFilter;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SearchAction extends BaseCmdAction {
	private String searchType = "";
	private String keywords = "";
	private String tid="";
	private String target="";

	@Override
	public Page doInit(WebForm form, Module module) {
		return doSearch(form, module);
	}
	
	public Page doSearch(WebForm form, Module module){
		searchType = CommUtil.null2String(form.get("type"));
		keywords = CommUtil.null2String(form.get("keywords"));
		tid = CommUtil.null2String(form.get("tid"));
		target = CommUtil.null2String(form.get("target"));
		String f_area=CommUtil.null2String(form.get("area"));
		String f_usage=CommUtil.null2String(form.get("usage"));
		HtmlInputFilter vFilter = new HtmlInputFilter();
		keywords = vFilter.filter(keywords);
		f_area = replaceBadWords(f_area);
		f_usage = replaceBadWords(f_usage);
		String addSql="k_search='0'";
		Collection<Object> paras=new ArrayList<Object>();
		if(tid.length()>0){
			paras.add(tid);
			addSql += " and k_type=?";
		}
		if(searchType.equals("")){
			if(keywords.length()>0){
				form.addResult("keys", keywords);
				keywords="%"+keywords+"%";
				paras.add(keywords);
				if(target.length()>0){
					form.addResult("target", target);
					addSql += " and k_"+target+" like ?";
				}else{
					paras.add(keywords);
					addSql += " and k_title like ? and k_content like ?";
				}
				addSql += " and k_display='1'";
			}else{
				form.addResult("msg", "关键字不能为空!");
				return new Page("index","index.htm","html","/web/" + thisSite.getTemplate() + "/");	
			}
		}else if(searchType.equals("field")){
			if(f_area.length()>0){
				paras.add(f_area);
				paras.add("area");
				addSql+=" and id in(Select k_newsid From k_fields Where k_value=? and k_name=?)";
			}
			if(f_usage.length()>0){
				paras.add(f_usage);
				paras.add("usage");
				addSql+=" and id in(Select k_newsid From k_fields Where k_value=? and k_name=?)";
			}
			if(f_area.length()+f_usage.length()==0){
				form.addResult("msg", "选项不能为空!");
				return new Page("index", "index.htm","html","/web/" + thisSite.getTemplate() + "/");
			}
		}
		List<com.knife.news.object.News> ret=newsDAO.getNews(addSql,paras);
		//System.out.println("查询出的长度为:"+ret.size());
		if(ret!=null){
			int rows = ret.size();// 分页开始
			int pageSize = 20;
			int currentPage = 1;
			int frontPage = 1;
			int nextPage = 2;
			List<com.knife.news.object.News> firstNews = new ArrayList<com.knife.news.object.News>();
			int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
			if(totalPage<1)totalPage=1;
			if(nextPage>totalPage){
				nextPage=totalPage;
			}
			firstNews = newsDAO.getNewsBySql(addSql+" order by k_order desc,k_date desc", paras,0, pageSize);
			//System.out.println("返回给模板引擎的关键字是:"+keywords);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("newsList", firstNews);
		}
		if(searchType.equals("field")){
			form.addResult("type", searchType);
			if(f_area.length()>0){
				form.addResult("r_area", f_area);
			}
			if(f_usage.length()>0){
				form.addResult("r_usage", f_usage);
			}
			com.knife.news.object.Type nowType = typeDAO.getTypeByName(CommUtil.null2String(form.get("typeName")));
			form.addResult("thisType", nowType);
			return new Page("search", "search_pro.htm","html","/web/" + thisSite.getTemplate()+"/");
		}else{
			//return module.findPage("index");
			return new Page("search", "search.htm","html","/web/" + thisSite.getTemplate()+"/");
		}
	}
	
	public Page doPage(WebForm form, Module module) {
		searchType = CommUtil.null2String(form.get("type"));
		keywords = CommUtil.null2String(form.get("keywords"));
		tid = CommUtil.null2String(form.get("tid"));
		target = CommUtil.null2String(form.get("target"));
		String f_area=CommUtil.null2String(form.get("area"));
		String f_usage=CommUtil.null2String(form.get("usage"));
		HtmlInputFilter vFilter = new HtmlInputFilter();
		keywords = vFilter.filter(keywords);
		f_area = replaceBadWords(f_area);
		f_usage = replaceBadWords(f_usage);
		String addSql="k_search='0'";
		Collection<Object> paras=new ArrayList<Object>();
		if(tid.length()>0){
			paras.add(tid);
			addSql += " and k_type=?";
		}
		if(searchType.equals("")){
			if(keywords.length()>0){
				form.addResult("keys", keywords);
				keywords="%"+keywords+"%";
				paras.add(keywords);
				if(target.length()>0){
					form.addResult("target", target);
					addSql += " and k_"+target+" like ?";
				}else{
					paras.add(keywords);
					addSql += " and k_title like ? and k_content like ?";
				}
				addSql += " and k_display='1' and k_type<>'25202599-3194843' ";
			}else{
				form.addResult("msg", "关键字不能为空!");
				return new Page("index", "index.htm","/web/" + thisSite.getTemplate()+"/");
			}
		}else if(searchType.equals("field")){
			if(f_area.length()>0){
				paras.add(f_area);
				paras.add("area");
				addSql+=" and id in(Select k_newsid From k_fields Where k_value=? and k_name=?)";
			}
			if(f_usage.length()>0){
				paras.add(f_usage);
				paras.add("usage");
				addSql+=" and id in(Select k_newsid From k_fields Where k_value=? and k_name=?)";
			}
			if(f_area.length()+f_usage.length()==0){
				form.addResult("msg", "选项不能为空!");
				return new Page("index","index.htm","html","/web/" + thisSite.getTemplate()+"/");
			}
		}
		//System.out.println("输出的完整sql:"+addSql+",此时参数个数为:"+paras.size());
		List<com.knife.news.object.News> allNews = newsDAO.getNews(addSql,paras);
		int rows = allNews.size();
		int pageSize = 20;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		if(paraPage<1){paraPage=1;}
		if(paraPage>totalPage){paraPage=totalPage;}
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<com.knife.news.object.News> newsList = new ArrayList<com.knife.news.object.News>();
		if (end < pageSize) {
			newsList = newsDAO.getNewsBySql(addSql+" order by k_order desc,k_date desc", paras, begin - 1, end);
		} else {
			newsList = newsDAO.getNewsBySql(addSql+" order by k_order desc,k_date desc", paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("rows", rows);
		form.addResult("newsList", newsList);
		if(searchType.equals("field")){
			form.addResult("type", searchType);
			if(f_area.length()>0){
				form.addResult("r_area", f_area);
			}
			if(f_usage.length()>0){
				form.addResult("r_usage", f_usage);
			}
			com.knife.news.object.Type nowType = typeDAO.getTypeByName(CommUtil.null2String(form.get("typeName")));
			form.addResult("thisType", nowType);
			return new Page("search", "search_pro.htm","html","/web/" + thisSite.getTemplate()+"/");
		}else{
			//return module.findPage("index");
			return new Page("search", "search.htm","html","/web/" + thisSite.getTemplate()+"/");
		}
	}
	
	private String replaceBadWords(String words){
		words = Jsoup.parse(words).text();
		words=words.replace("<", "");
		words=words.replace(">", "");
		words=words.replace("%", "");
		return words;
	}
}