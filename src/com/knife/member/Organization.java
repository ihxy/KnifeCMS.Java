package com.knife.member;

/**
 * Organization entity. @author MyEclipse Persistence Tools
 */

public class Organization implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -6742837334762124910L;
	private Integer id;
	private String type;
	private Integer classid;

	// Constructors

	/** default constructor */
	public Organization() {
	}

	/** full constructor */
	public Organization(String type, int class_) {
		this.type = type;
		this.classid = class_;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getClassid() {
		return this.classid;
	}

	public void setClassid(int class_) {
		this.classid = class_;
	}

}