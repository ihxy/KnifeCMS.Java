package com.knife.member;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Example;
import org.hibernate.transform.ResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for Point
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see com.knife.member.Point
 * @author MyEclipse Persistence Tools
 */

public class PointDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(PointDAO.class);
	// property constants
	public void save(Point transientInstance) {
		log.debug("saving Point instance");
		try {
			getSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Point persistentInstance) {
		log.debug("deleting Point instance");
		try {
			getSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Point findById(java.lang.Integer id) {
		log.debug("getting Point instance with id: " + id);
		try {
			Point instance = (Point) getSession().get("com.knife.member.Point",
					id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Point instance) {
		log.debug("finding Point instance by example");
		try {
			List results = getSession()
					.createCriteria("com.knife.member.Point")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Point instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Point as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Point> findByMemGUID(Object memguid) {
		return ((List<Point>)findByProperty("memguid", memguid));
	}

//	public List findPointMainByMemGUID(Object memguid) {
//		try {
//			String queryString = "SELECT distinct(pointtype),convert(varchar(50),convert(decimal(19,2),isnull((SELECT SUM(case when EffectDate<>'' and EffectDate is not null then point else 0 end) FROM h_point  WHERE pointtype = a.pointtype and memguid=?),0))) AS point,convert(varchar(50),convert(decimal(19,2),isnull((SELECT SUM(case when EffectDate<>'' and EffectDate is not null then sjpoint else 0 end) FROM h_point  WHERE pointtype = a.pointtype and memguid=?),0))) AS sjpoint FROM h_Point as a";
//			Query queryObject = getSession().createQuery(queryString);
//			queryObject.setParameter(0, memguid);
//			queryObject.setParameter(1, memguid);
//			return queryObject.list();
//		} catch (RuntimeException re) {
//			log.error("find by property name failed", re);
//			throw re;
//		}
//	}
	
	public List findPointMainByMemGUID(Object memguid) {
		try {
			String queryString = "SELECT distinct(pointtype) as pointtype,ifnull((SELECT SUM(case when EffectDate<>'' and EffectDate is not null then point else 0 end) FROM point  WHERE pointtype = a.pointtype and memguid=?),0) AS ljjlpoint,ifnull((SELECT SUM(case when EffectDate<>'' and EffectDate is not null then sjpoint else 0 end) FROM point  WHERE pointtype = a.pointtype and memguid=?),0) AS ljsjpoint FROM point as a where memguid=?";
			SQLQuery pointQuery = getSession().createSQLQuery(queryString);
			pointQuery.setParameter(0, memguid);
			pointQuery.setParameter(1, memguid);
			pointQuery.setParameter(2, memguid);
			pointQuery.addEntity(PointSum.class);
			List list = pointQuery.list();
			return list;
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List findAll() {
		log.debug("finding all Point instances");
		try {
			String queryString = "from Point";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Point merge(Point detachedInstance) {
		log.debug("merging Point instance");
		try {
			Point result = (Point) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Point instance) {
		log.debug("attaching dirty Point instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Point instance) {
		log.debug("attaching clean Point instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}