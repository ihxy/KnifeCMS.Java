package com.knife.member;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Usertype entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.knife.member.Usertype
 * @author MyEclipse Persistence Tools
 */

public class UsertypeDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory
			.getLogger(UsertypeDAO.class);
	// property constants
	public static final String NAME = "name";
	public static final String RIGHTS = "rights";
	public static final String PARENT = "parent";
	public static final String ONSCORE = "onscore";
	public static final String MACNUM = "macnum";
	public static final String ORDER = "order";

	public void save(Usertype transientInstance) {
		log.debug("saving Usertype instance");
		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Usertype persistentInstance) {
		log.debug("deleting Usertype instance");
		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Usertype findById(java.lang.Integer id) {
		log.debug("getting Usertype instance with id: " + id);
		try {
			getSession().clear();
			Usertype instance = (Usertype) getSession().get("com.knife.member.Usertype", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Usertype instance) {
		log.debug("finding Usertype instance by example");
		try {
			List results = getSession().createCriteria("com.knife.member.Usertype")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Usertype instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from com.knife.member.Usertype as model where model."
					+ propertyName + "= ? order by model.order desc";
			Query queryObject=null;
			if(getSession()!=null){
				queryObject = getSession().createQuery(queryString);
				queryObject.setParameter(0, value);
			}
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List findByRights(Object rights) {
		return findByProperty(RIGHTS, rights);
	}
	
	public List findByParent(Object parent) {
		return findByProperty(PARENT, parent);
	}
	
	public List findByOnscore(Object onscore) {
		return findByProperty(ONSCORE, onscore);
	}
	
	public List findByMacnum(Object macnum) {
		return findByProperty(ONSCORE, macnum);
	}
	
	public List findByOrder(Object order) {
		return findByProperty(ORDER, order);
	}

	public List findAll() {
		log.debug("finding all Usertype instances");
		try {
			String queryString = "from com.knife.member.Usertype";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Usertype merge(Usertype detachedInstance) {
		log.debug("merging Usertype instance");
		try {
			Usertype result = (Usertype) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Usertype instance) {
		log.debug("attaching dirty Usertype instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Usertype instance) {
		log.debug("attaching clean Usertype instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}