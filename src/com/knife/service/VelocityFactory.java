/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.knife.service;

import java.io.File;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

import com.knife.web.Globals;
/**
 *
 * @author Administrator
 */
public class VelocityFactory {
	private static final Logger logger = (Logger) Logger
			.getLogger(VelocityFactory.class);
	private static VelocityEngine vm;
	private static VelocityEngine vs;
	private static VelocityEngine ve;
	private String templatepath;
	private static String managetemplatepath;
	private static String systemplatepath;
	private static String siteTemplatePath;
	private static Template template;
	private static Properties pm;
	private static Properties ps;
	private static Properties p;

	public VelocityFactory() {
		templatepath = Globals.APP_BASE_DIR + "WEB-INF/templates/web/";
		if (templatepath.indexOf('/') > 0) {
			templatepath = templatepath.replace('/', File.separatorChar);
		}
	}
	
	private static VelocityEngine getManageEngine() {
		managetemplatepath= Globals.APP_BASE_DIR + "WEB-INF/templates/manage/";
		if (managetemplatepath.indexOf('/') > 0) {
			managetemplatepath = managetemplatepath.replace('/', File.separatorChar);
		}
		if (vm == null){
			pm = new Properties();
			vm = new VelocityEngine();
			pm.setProperty("file.resource.loader.path", managetemplatepath);
			try {
				vm.init(pm);
			} catch (Exception e) {
				System.out.println("初始化错误：" + e.getMessage());
			}
		}
		return vm;
	}
	
	private static VelocityEngine getSysEngine() {
		systemplatepath= Globals.APP_BASE_DIR + "WEB-INF/templates/sys/";
		if (systemplatepath.indexOf('/') > 0) {
			systemplatepath = systemplatepath.replace('/', File.separatorChar);
		}
		if (vs == null){
			ps = new Properties();
			vs = new VelocityEngine();
			ps.setProperty("file.resource.loader.path", systemplatepath);
			try {
				vs.init(ps);
			} catch (Exception e) {
				System.out.println("初始化错误：" + e.getMessage());
			}
		}
		return vs;
	}

	public Template getTemplate(String fileName) {
		return getTemplate(fileName, "utf-8");
	}
	
	public static Template getManageTemplate(String fileName){
		try {
			template = getManageEngine().getTemplate(fileName,"utf-8");
		} catch (Exception e) {
			logger.error(e.getClass().toString() + e);
		}
		return template;
	}
	
	public static Template getSysTemplate(String fileName){
		try {
			template = getSysEngine().getTemplate(fileName,"utf-8");
		} catch (Exception e) {
			logger.error(e.getClass().toString() + e);
		}
		return template;
	}
	
	public Template getTemplate(String fileName, String encoding){
		return getTemplate("default", fileName, encoding);
	}

	public Template getTemplate(String siteTemplate, String fileName, String encoding) {
		siteTemplatePath = templatepath + siteTemplate + "/";
		try {
			ve = new VelocityEngine();
			p = new Properties();
			p.setProperty("file.resource.loader.path", siteTemplatePath);
			p.setProperty("tools.view.servlet.layout.directory", siteTemplatePath+"layout/");
			p.setProperty("tools.view.servlet.layout.default.template", "layout.htm");
			ve.init(p);
			template = ve.getTemplate(fileName, encoding);
		} catch (Exception e) {
			//System.out.println("sssssssssssssss:"+systemplatepath);
			template = getSysEngine().getTemplate("no.htm","utf-8");
			logger.error(e.getClass().toString() + e);
			//throw new NullPointerException("模板错误:模板路径"+siteTemplatePath+","+e.getMessage());
		}
		return template;
	}
}