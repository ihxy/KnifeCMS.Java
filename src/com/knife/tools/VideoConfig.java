package com.knife.tools;

import java.io.File;

import com.knife.web.Globals;

public class VideoConfig {
	private static String xml_path = Globals.APP_BASE_DIR + "moviemasher" + File.separator + "media";

	public static String getConfig(String path) {
		String xml_content="";
		xml_path = Globals.APP_BASE_DIR + "moviemasher" + File.separator + "media";
		xml_path += File.separator+"xml"+File.separator+"media_" + path + ".xml";
		try {
			//System.out.println(xml_path);
			File configFile=new File(xml_path);
			if(configFile.canRead()){
				xml_content = org.apache.commons.io.FileUtils.readFileToString(
						configFile, "UTF-8");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml_content;
	}
}
