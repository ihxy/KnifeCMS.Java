package com.knife.tools;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class CopyOfSessionUtil extends HttpServlet implements HttpSessionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("rawtypes")
	public static java.util.HashMap sessionl = new java.util.HashMap();

	// Notification that a session was created
	@SuppressWarnings("unchecked")
	public void sessionCreated(HttpSessionEvent se) {
		//System.out.println("新创建 sessionid is " + se.getSession().getId());
		// 把session的引用放到一个全局静态变量里
		sessionl.put(se.getSession().getId(), se.getSession());
	}

	// Notification that a session was invalidated
	public void sessionDestroyed(HttpSessionEvent se) {
		//System.out.println("被释放的 sessionid is " + se.getSession().getId());
		// 把session的引用移出，好让系统可以及时释放内存
		sessionl.remove(se.getSession());
		try{
			se.getSession().removeAttribute("loginuser");
		}catch(Exception e){
		}
	}
}