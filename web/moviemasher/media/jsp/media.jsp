<%@ page language="java" pageEncoding="UTF-8" import="com.knife.news.model.Constants,com.knife.tools.VideoConfig"%>
<%@ include file="/include/manage/checkUser.jsp" %><%
int count = 10;
String video="";
if(request.getParameter("count")!=null){
	count = Integer.parseInt(request.getParameter("count").toString());
}
int index = 0;
if(request.getParameter("index")!=null){
	index = Integer.parseInt(request.getParameter("index").toString());
}
String group = "";
if(request.getParameter("group")!=null){
	group = request.getParameter("group");
}
String xml_content="";
if (group.length()>0)
{
	xml_content = VideoConfig.getConfig(group);
}

if(session.getAttribute("video")!=null){
	video=session.getAttribute("video").toString();
	//video="/moviemasher/media/jsp/getVideo.jsp?v="+video;
}
if(video.length()>=0 && group.equals("video")){
	session.setAttribute("permission","1");
	out.println("<moviemasher>");
	out.print("<media group=\"video\" type=\"video\" id=\"defaultvideo\"");
	out.print(" label=\"default\" duration=\"97\"");
	out.print(" audio=\""+video+"\" url=\""+video+"\"");
	out.print(" fill=\"crop\" editable=''/>");
	out.println("</moviemasher>");
}else{
	out.println(xml_content);
}%>