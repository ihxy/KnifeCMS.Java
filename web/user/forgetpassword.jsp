<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);

Userinfo auser=null;
String login_email="";
String login_acount="";
if(request.getCookies()!=null){
	Cookie[] allCookie = request.getCookies();
	for(int i=0;i<allCookie.length;i++){
		if(allCookie[i].getName().equals("loginuser")){
			login_email=allCookie[i].getValue();
		}
	}
}
String url="";
if(request.getParameter("url")!=null){
	url=request.getParameter("url");
	//session.setAttribute("url",url);
	if(url.indexOf("/")!=0){
		url="";
	}
}

//生成时间戳
Date date = new Date();
long time = date.getTime();
String dateline = time + "";
dateline = dateline.substring(0, 10);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>用户登陆</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0">
	<link rel="stylesheet" href="skin/css/style.css" type="text/css" media="all" />
	<style type="text/css">
	#loginForm{
		margin:0px auto;
		width:1000px;
		padding:80px 0px;
		text-align:center;
		background:#C7D7E7;
	}
	#loginform{
		width:1000px;
	}
	#loginForm a{
		color:gray;
	}
	#loginForm input{
		border:1px solid gray;
	}
	#saveUserInfo{
		border:0!important;
	}
	.inputText input{
		width:150px;
	}
	</style>
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
	<link rel="stylesheet" href="skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
<!--
jQuery.noConflict(true);

	if(self.parent.frames.length!=0){
		self.parent.location=document.location.href;
	}
	jQuery(function(){
		try{
			var mac_str=document.getElementById('GetMAC').GetMac;
			jQuery("#mac").val(mac_str);
		}catch(error){}
		jQuery("#loginform").validationEngine();
	});
	
	function checkForm(){
		jQuery("#loginform")[0].submit();
	}
	
function reloadImg(id) {
   var obj = document.getElementById(id);
   var date = new Date();
   obj.src = '/valicode?v=' + date.getTime();
   return false;
}
-->
	</script>
  </head>

<body>
<div class="container">
	<%@include file="inc/head.jsp" %>
	<div class="clearfloat"></div>
	<div id="loginForm">
			<form id="loginform" name="loginform" action="/user/opt/sendmail.jsp" method="post">
			<table width="350" align="center" cellspacing="0" cellpadding="0" border="0" style="border:1px solid gray;text-align:center;font-size:12px;background:white">
				<tr><td style="background:#12335F;color:white;border-bottom:1px solid gray;height:32px;font-size:14px;">忘记密码</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="inputText">
						邮箱帐号：<input id="email" type="text" name="email" class="validate[required,length[0,20]]" />
					<input style="width:42px" name="submit" type="submit" value="提交" /></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						请您正确输入邮箱账号，密码将会发送至您的邮箱账号。
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td style="background:#12335F;color:white;border-top:1px solid gray">&nbsp;</td></tr>
			</table>
			</form>
		</div>
	
	<%@include file="inc/foot.jsp" %>
</div>
</body>
</html>