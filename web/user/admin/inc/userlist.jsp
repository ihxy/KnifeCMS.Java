<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
request.setCharacterEncoding("UTF-8");
int isfee=0;
if(request.getParameter("isfee")!=null){
	isfee=Integer.parseInt(request.getParameter("isfee"));
}

int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_next=page_now;
}
String key_realname="";
String key_email="";
String key_acount="";
if(request.getParameter("key_realname")!=null){
	key_realname = request.getParameter("key_realname");
}
if(request.getParameter("key_email")!=null){
	key_email = request.getParameter("key_email");
}
if(request.getParameter("key_acount")!=null){
	key_acount = request.getParameter("key_acount");
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>会员列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="userlist.jsp" method="post">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<th>搜索</th>
			<th>会员账号：<input type="text" name="key_acount" value="<%=key_acount%>" /></th>
			<th>会员姓名：<input type="text" name="key_realname" value="<%=key_realname%>" /></th>
			<th>邮件地址：<input type="text" name="key_email" value="<%=key_email%>" /></th>
			<th><input type="submit" name="submit" value="查询" /></th>
		</tr>
	</table>
</form>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
				<td>编号</td>
				<td>会员帐号</td>
				<td>会员姓名</td>
				<td>邮件地址</td>
				<td>积分</td>
				<td>操作</td>
		</tr>
				<%
				UserinfoDAO userDAO=new UserinfoDAO();
				List users=new ArrayList();
				if(key_email.length()>0){
					users=userDAO.findByProperty("email",key_email);
					total_page=1;
				}else if(key_realname.length()>0){
					users=userDAO.findByProperty("realname",key_realname);
					total_page=1;
				}else if(key_acount.length()>0){
					users=userDAO.findByProperty("acount",key_acount);
					total_page=1;
				}else{
					if(isfee>0){
						users=userDAO.findPagedAllisfee(page_now,20,isfee);
						total_page = userDAO.getTotalPageIsFee(20,isfee);
					}else if(isfee<0){
						users=userDAO.findPagedAllNew(page_now,20);
						total_page = userDAO.getTotalPageNew(20);
					}else{
						users=userDAO.findPagedAll(page_now,20);
						total_page = userDAO.getTotalPage(20);
					}
				}
				if(page_now>1)page_prev=page_now-1;
				if(page_now<total_page)page_next=page_now+1;
				for(Object aobj:users){
					Userinfo auser=(Userinfo)aobj;
				%>
				<tr>
					<td><%=auser.getId()%></td>
					<td><%=auser.getAcount()%></td>
					<td><%=auser.getRealname()%></td>
					<td><%=auser.getEmail()%></td>
					<td><%=auser.getScore()%></td>
					<td><%
					if(!"admin".equals(auser.getAcount())){
					%><a href="useredit.jsp?uid=<%=auser.getId()%>">编辑</a> | <a href="/user/opt/deluser.jsp?uid=<%=auser.getId()%>">删除</a>
					<%}%>
					</td>
				</tr>
				<%}%>
				<tr>
				<td colspan="6" style="height:32px" align="center">
					<a href="?page=1&isfee=<%=isfee%>"><img src="/user/skin/images/page_first.gif" border="0" /></a>
					<a href="?page=<%=page_prev%>&isfee=<%=isfee%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
					<%
					int begin=page_now-2;
					if(begin<1){begin=1;}
					int end=begin+5;
					if(end>(total_page+1)){
						end=total_page+1;
						begin=end-5;
						if(begin<1){begin=1;}
					}
					for(int i=begin;i<end;i++){
						if(i==page_now){
							%>
							<a href="?page=<%=i%>&isfee=<%=isfee%>"><font color=red><%=i%></font></a>
							<%
						}else{
							%>
							<a href="?page=<%=i%>&isfee=<%=isfee%>"><%=i%></a>
							<%
						}
					}
					%>
					<a href="?page=<%=page_next%>&isfee=<%=isfee%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
					<a href="?page=<%=total_page%>&isfee=<%=isfee%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
				</td>
			</tr>
	</table>
</body>
</html>