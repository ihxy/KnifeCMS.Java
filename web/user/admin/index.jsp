<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="inc/checkadmin.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>资料库 - 后台管理</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="0"/>
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
		<meta http-equiv="description" content="This is my page"/>
		<link rel="stylesheet" href="/user/skin/css/style.css" type="text/css"
			media="all" />
		<style type="text/css">
.user_panel {
	width: 1000px;
	font-size: 12px;
	margin: 8px 4px;
}

.user_menu {
	border: 1px solid gray;
	font-size: 12px;
	margin-bottom: 8px;
}

.user_menu th {
	height: 24px;
	font-weight: normal;
	text-align: left;
	text-indent: 12px;
	border-bottom: 1px solid gray;
}

.menu_list {
	margin: 10px 45px;
}

.user_main {
	width: 750px;
}
</style>
		<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
		<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	</head>
	<body>
		<div class="container">
			<%@include file="inc/head.jsp"%>
			<div class="user_panel">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="205" valign="top">
							<table class="user_menu" width="205" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										资料库管理
									</th>
								</tr>
								<tr>
									<td>
										<div class="menu_list">
											<ul>
												<li>
													<a href="/user/admin/inc/datalist.jsp" target="main">资料列表</a>
												</li>
												<li>
													<a href="/user/admin/inc/dataadd.jsp" target="main">添加资料</a>
												</li>
												<li>
													<a href="/user/admin/inc/datatype.jsp" target="main">资料类别</a>
												</li>
												<li>
													<a href="/user/admin/inc/risktype.jsp" target="main">风险类别</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="205" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										学习管理
									</th>
								</tr>
								<tr>
									<td>
										<div class="menu_list">
											<ul>
												<li>
													<a href="/user/admin/inc/causelist.jsp" target="main">课程管理</a>
												</li>
												<li>
													<a href="/user/admin/inc/note.jsp" target="main">笔记管理</a>
												</li>
												<li>
													<a href="/user/admin/inc/choosecause.jsp" target="main">选购课程</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="205" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										会议管理
									</th>
								</tr>
								<tr>
									<td>
										<div class="menu_list">
											<ul>
												<li>
													<a href="http://www.zd211.com/login.aspx?siteid=tyfy" target="_blank">会议管理(诚迅通)</a>
												</li>
												<li>
													<a href="/user/admin/inc/meetinglist.jsp" target="main">会议列表</a>
												</li>
												<li>
													<a href="/user/admin/inc/meetingadd.jsp" target="main">添加会议</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="205" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										会员管理
									</th>
								</tr>
								<tr>
									<td>
										<div class="menu_list">
											<ul>
												<li>
													<a href="/user/admin/inc/userlist.jsp" target="main">会员列表</a>
												</li>
												<li>
													<a href="/user/admin/inc/useradd.jsp" target="main">添加会员</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>
							
							<table class="user_menu" width="205" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										访问统计
									</th>
								</tr>
								<tr>
									<td>
										<div class="menu_list">
											<ul>
												<li>
													<a href="/user/statservlet?type=dayStat&amp;dayCounts=5" target="main">日流量统计</a>
												</li>
												<li>
													<a href="/user/statservlet?type=monthStat&amp;monthCounts=5" target="main">月流量统计</a>
												</li>
												<li>
													<a href="/user/statservlet?type=hourStat&amp;dayCounts=5&amp;count=4" target="main">访问时段分布统计</a>
												</li>
												<li>
													<a href="/user/statservlet?type=browserStat" target="main">访客浏览器统计</a>
												</li>
												<li>
													<a href="/user/statservlet?type=osStat" target="main">访客操作系统统计</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>
						</td>
						<td width="*" align="right" valign="top">
							<div class="user_main">
								<iframe name="main" id="main" width="750" height="487"
									src="/user/admin/inc/datalist.jsp" frameborder="0"></iframe>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>
