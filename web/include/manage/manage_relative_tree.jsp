<%@ page language="java"  pageEncoding="UTF-8"  import="com.knife.news.model.Constants,com.knife.news.object.User,com.knife.util.CommUtil,com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News"%>
<%@ include file="checkUser.jsp" %>
<%
String tree_id="1";
if(request.getParameter("tree_id")!=null){
	tree_id=CommUtil.null2String(request.getParameter("tree_id"));
}
String treenews="";
if(request.getParameter("treenews")!=null){
	treenews=CommUtil.null2String(request.getParameter("treenews"));
}
String newsid="";
if(request.getParameter("newsid")!=null){
	newsid=CommUtil.null2String(request.getParameter("newsid"));
}
String treeTitle="所有文章";
String fullTreeTitle="列出所有文章";
if(treenews.length()>0){
	NewsService newsDAO = NewsServiceImpl.getInstance();
	News news = newsDAO.getNewsById(treenews);
	treeTitle = news.getTitle(9);
	fullTreeTitle = news.getTitle();
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>文章分类树</title>
		<link rel="stylesheet" href="/include/manage/zTreeStyle/zTreeStyle.css" type="text/css" />
<style type="text/css">
body {
	padding: 0px;
	margin: 0px;
	font-size:12px;
	overflow-x:hidden;
	overflow-y:auto;
}
#treeTitle{
	font-weight:bold;
	background:url('/include/manage/images/main/tab_05.gif');
	/*width:165px;*/
	height:28px;
	margin:3px;
	margin-bottom:0;
	border-left:1px solid #b5d6e6;
	border-right:1px solid #b5d6e6;
	text-indent:15px;
}
#treeTitle a{line-height:28px;color:black;text-decoration:none}
#treeList {
	margin:3px;
	margin-top:0;
	/*width:165px;*/
	height:482px;
	border:1px solid #b5d6e6;
	border-top:0;
	overflow:hidden;
	overflow-y:auto;
}
.tree{padding:0;}
/* ------------- 右键菜单 -----------------  */

div#rMenu {
	background-color:#F9F9F9;
	text-align: left;
	width:72px;
	border-top:1px solid #F1F1F1;
	border-left:1px solid #F1F1F1;
	border-right:1px solid gray;
	border-bottom:1px solid gray;
}

div#rMenu ul {
	list-style: none outside none;
	margin:0px 1px;
	padding:0;
}
div#rMenu ul li {
	cursor: pointer;
	width:70px;
	height:20px;
	line-height:20px;
	text-indent:8px;
	margin:1px 0px;
	font-size:12px;
	border-bottom:1px solid gray;
}
div#rMenu ul li a{display:block;width:100%;text-decoration:none;color:black}
div#rMenu ul li a:hover{
	color:white;
	background-color:darkblue;
}
div#rMenu ul li a:visited{color:black}
</style>
<script type="text/javascript" src="/include/manage/js/jquery.js"></script>
<script type="text/javascript" src="/include/manage/js/jquery-ztree.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--
$.ajaxSetup ({  
	cache: false
}); 

function clone(jsonObj, newName) {
    var buf;
    if (jsonObj instanceof Array) {
        buf = [];
        var i = jsonObj.length;
        while (i--) {
            buf[i] = clone(jsonObj[i], newName);
        }
        return buf;
    }else if (typeof jsonObj == "function"){
        return jsonObj;
    }else if (jsonObj instanceof Object){
        buf = {};
        for (var k in jsonObj) {
	        if (k!="parentNode") {
	            buf[k] = clone(jsonObj[k], newName);
	            if (newName && k=="name") buf[k] += newName;
	        }
        }
        return buf;
    }else{
        return jsonObj;
    }
}
  
	var zTree1;
	var setting;

	setting = {
		async: true,
		<%
		if(!tree_id.equals("1")){
		%>
		asyncUrl: "getTree.jsp?tree_id=<%=tree_id%>",
		<%
		}else{
		%>
		asyncUrl: "getTree.jsp",
		<%
		}
		%>
		asyncParam: ["name","id","siteId"],
		callback: {
			click: zTreeOnClick
		}
	};

	var zNodes =[{siteId:""}];

	$(document).ready(function(){
		refreshTree();
	});
	
	function expandAll(expandSign) {
		zTree1.expandAll(expandSign);
	}

	function refreshTree() {
		zTree1 = $("#treeList").zTree(setting);
	}
	
	function zTreeOnClick(event, treeId, treeNode){
		window.parent.frames["config<%=tree_id%>"].location.href="/manage_relative.ad?parameter=list&sid="+treeNode.siteId+"&newsid=<%=newsid%>&treenews=<%=treenews%>&tid=" + treeNode.id;
	}
  //-->
</SCRIPT>
	</head>
<body>
<div id="treeTitle"><img src="/include/manage/zTreeStyle/img/sim/tree.png" align="absmiddle"/>&nbsp;<a href="/manage_relative.ad?parameter=list&newsid=<%=newsid%>&treenews=<%=treenews%>" target="config<%=tree_id%>" title="<%=fullTreeTitle%>"><%=treeTitle%></a></div>
<div id="treeList" class="tree"></div>
	</body>
</html>