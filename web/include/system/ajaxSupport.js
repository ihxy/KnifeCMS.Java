﻿function newXMLHttpRequest() {
  var xmlreq = false;
  if (window.XMLHttpRequest) {   
    xmlreq = new XMLHttpRequest();
  } else if (window.ActiveXObject) {    
    try {      
      xmlreq = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e1) {      
      try {       
        xmlreq = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {      
      }
    }
  }
  return xmlreq;
}
function newXmlDom(req)
{
var dom = null;
  if (typeof(DOMParser) != "undefined") { 
  var dp= new DOMParser();  
  dom=dp.parseFromString(req.responseText, "text/xml" );
  } else { // IE
    try { dom = new ActiveXObject("MSXML2.DOMDocument");} catch (e) { }
    if (dom == null) try { dom = new ActiveXObject("Microsoft.XMLDOM"); } catch (e) { }
   if(dom!=null) dom.loadXML(req.responseText);   	
  }
return dom;
}
//处理返回信息
//xmlHttp返回值,
//method:方法名 方法必须带一个参数如doRecive(xNode);
function handleAjaxResult(req,method) { 
  return function () {  
    if (req.readyState == 4) {     
      if (req.status == 200) {
      // 将载有响应信息的XML传递到处理函数
	   var objXMLDoc=newXmlDom(req); 
//alert(objXMLDoc.xml);  
       eval("if(objXMLDoc.firstChild)"+method+"(objXMLDoc.firstChild.nextSibling);"); 
      } else {       
        //alert("HTTP error: "+req.status);
      }
    }
  }
}
//执行客户端Ajax命令
//url 数据post地址
//postData 发送的数据包
//handleMethod　处理返回的方法
function executeAjaxCommand(url,postData,handleMethod)
{
   var req = newXMLHttpRequest(); 
   req.onreadystatechange =handleAjaxResult(req,handleMethod);    
   req.open("POST", url, true); 
   req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   req.setRequestHeader("charset","UTF-8");  
   req.send(postData);
//alert("已发送");
}