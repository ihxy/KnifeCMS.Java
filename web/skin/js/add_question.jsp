﻿<%@ page language="java" import="java.util.*,com.knife.util.CommUtil,com.knife.news.object.*,
com.knife.news.logic.*,com.knife.news.logic.impl.*,com.knife.news.model.Form,com.knife.news.model.Fields" pageEncoding="UTF-8"%>
<%
	String tid=CommUtil.null2String(request.getParameter("tid"));
	String title=CommUtil.null2String(request.getParameter("title"));
	String summary=CommUtil.null2String(request.getParameter("content"));
	Date startDate=new Date();
	Date endDate=null;
	News fnews = new News();
	fnews.setType(tid);
	fnews.setTitle(title);
	fnews.setContent(summary);
	fnews.setDate(startDate);
	fnews.setOffdate(endDate);
	fnews.setDisplay(0);
	NewsService newsDAO = NewsServiceImpl.getInstance();
	TypeService typeDAO = TypeServiceImpl.getInstance();
	FormService formDAO = FormServiceImpl.getInstance();
	FieldsService fieldsDAO = FieldsServiceImpl.getInstance();
	String id = newsDAO.addNews(fnews);
	
	News news=new News();
		if (id.length()>0) {
			news=newsDAO.getNewsById(id);
			// 保存自定义表单
			Type aType = typeDAO.getTypeById(tid);
			if (aType.getForm() != null) {
				String formId = aType.getForm();
				if (formId.length() > 0) {
					try{
						Form aform = formDAO.getFormById(formId);
						List<FormField> formFields = fieldsDAO.jsonStringToList(
								news.getId(), aform.getContent());
						for (FormField formField : formFields) {
							Fields afield = new Fields();
							afield.setFormid(formId);
							afield.setName(formField.getName());
							afield.setNewsid(id);
							afield.setOrder(formField.getOrder());
							afield.setValue(CommUtil.null2String(request.getParameter("u_"
									+ formField.getName())));
							fieldsDAO.saveFields(afield);
							//System.out.println("成功添加字段值:"+formField.getName());
						}
					}catch(Exception e){
						out.println("自定义表单查找异常:"+e.getMessage());
					}
				}
			}
		}
%>
<script>
alert("提问成功，请耐心等待管理员答复!");
history.back(-1);
</script>